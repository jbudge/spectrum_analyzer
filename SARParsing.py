#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 22 15:21:57 2020

@author: jeff

SAR file read/write object. Use this to interact with a SAR file.
"""

import numpy as np
from pathlib import Path
from os.path import exists
import os
from tqdm import tqdm
import mmap
import struct
import pandas as pd
from scipy.signal.windows import taylor
from scipy.interpolate import CubicSpline
from shutil import copy
import array
from re import search
import pickle
import matplotlib.pyplot as plt
from typing import Union
import time
from xml.etree.ElementTree import ElementTree, Element, SubElement, indent, tostringlist
from xml.dom.minidom import parse
import sys

freq_offset = 5e6
DTR = np.pi / 180
mstoknots = 1.94384
c0 = 299792458.0
TAC = 125e6
fs = 2e9

SHRT_MAX = 32767
USHRT_MAX = 65535
INT_MAX = 4294967295
pulseChunkMax = USHRT_MAX * 10000  # No more than 10000 full size pulses per loop
DSCALE = .75 * SHRT_MAX  # * 2e16
P_CONV = -23.142857 / 3600
T_CONV = 11.571429 / 3600
DATA_START_WORD = b'\x0A\xB0\xFF\x18'
GPS_START_WORD = b'\x0A\xFF\xB0\x18'
GIMBAL_START_WORD = b'\x0A\xFF\x18\xB0'
STOP_WORD = b'\xAA\xFF\x11\x55'
COLLECTION_MODE_DIGITAL_CHANNEL_MASK = 0x000003E000000000
COLLECTION_MODE_OPERATION_MASK = 0x1800000000000000
COLLECTION_MODE_DIGITAL_CHANNEL_SHIFT = 37
COLLECTION_MODE_OPERATION_SHIFT = 59
CAL_SAR_SHIFT = 1024
REC_CHUNK_SZ = 4096
SDR_HEADER_SZ = 28
SAR_HEADER_SZ = 24


def isSAR(fnme):
    files = findAllFilenames(fnme)
    xml = loadXMLFile(files['xml'], True)
    if 'SlimSAR_Configuration' in xml:
        return True
    else:
        return False


def readDataHeader(data, is_slimsar=False):
    frm = int.from_bytes(data[:4], byteorder='big', signed=False)
    syst = int.from_bytes(data[4:8], byteorder='big', signed=False)
    if is_slimsar:
        mode = int.from_bytes(data[8:10], byteorder='big', signed=False)
        nsam = int.from_bytes(data[10:12], byteorder='big', signed=False)
        att = data[21]
        is_cal = True if ((mode & 0x0E00) >> 9) == 0 else False
        channel = mode
    else:
        mode = int.from_bytes(data[8:16], byteorder='big', signed=False)
        # Calc channel number
        channel = (mode & COLLECTION_MODE_DIGITAL_CHANNEL_MASK) >> COLLECTION_MODE_DIGITAL_CHANNEL_SHIFT
        is_cal = True if ((mode & COLLECTION_MODE_OPERATION_MASK) >> COLLECTION_MODE_OPERATION_SHIFT) == 1 else False
        nsam = int.from_bytes(data[16:20], byteorder='big', signed=False)
        # Att is only 5 bits so we mask it
        att = data[20] & 0x1f
        # Rest is AGC/reserved data stuff, ignore it
    return frm, syst, is_cal, att, nsam, channel


def getSARMode(mode):
    is_cal = True if ((mode & 0x0E00) >> 9) == 1 else False
    is_dechirp = (mode & 0x2000) >> 13
    is_inphase = (mode & 0x1000) >> 12 if is_dechirp else 0
    band_num = 'Band {}'.format(((mode & 0x8000) >> 15) + 1)
    ant_num = ((mode & 0x0007) >> 0) + 1
    rx_path = (mode & 0x4000) >> 14
    rx_path = 'J1' if rx_path == 1 else 'J2'
    pol = (mode & 0x0018) >> 3
    pol = 'HH' if pol == 0 else 'HV' if pol == 1 else 'VH' if pol == 2 else 'VV'
    return ant_num, rx_path, pol, band_num, is_dechirp, is_inphase, is_cal


class SARParse(object):

    def __init__(self, fnme=None, debug_dir='/home/jeff/repo/Debug', pol=None, get_cov=False):
        # Get the file we'll be parsing, and some extra data for it
        self.fnme = fnme
        self.files = findAllFilenames(fnme, debug_dir)
        self.asi = None
        self.xml = loadXMLFile(self.files['xml'], True)['SlimSAR_Configuration']['SlimSAR_Info']
        self.n_channels = int(self.xml['Collection_Mode_Information']['Num_Collection_Modes'])
        self.perc_complete = 0
        channels = []

        # Get channel specific data for each channel
        for channel in range(self.n_channels):
            ch_settings = self.xml['Collection_Mode_Information']['Collection_Mode_{}'.format(channel)]
            band_settings = self.xml[
                'Band_{}'.format(int(self.xml['Collection_Mode_Information']['Collection_Mode_{}'.format(channel)]
                                     ['Collection_Mode_{}_Band_Num'.format(channel)][-1]))]
            channels.append(Channel(ch_settings=ch_settings, band_settings=band_settings, is_slimsar=True))
            self.pol = pol

        try:
            fl_len = np.sqrt(
                (self.xml['Flight_Line']['Start_Latitude_D'] - self.xml['Flight_Line']['Stop_Latitude_D']) ** 2 +
                (self.xml['Flight_Line']['Start_Longitude_D'] - self.xml['Flight_Line']['Stop_Longitude_D']) ** 2)
            self.flight_len = fl_len / (self.xml['Common_Band_Settings']['Velocity_Knots'] * .514444)
        except KeyError:
            self.flight_len = 4

        timesync_n = 0
        gps_df = pd.DataFrame(columns=['lat', 'lon', 'alt',
                                       'vn', 've', 'vu', 'r', 'p', 'y', 'azimuthX', 'azimuthY', 'systime',
                                       'corr_systime', 'gps_wk', 'ptype'])
        timesync = pd.DataFrame(columns=['week', 'secs', 'systime', 'corr_systime'])
        gimbal_df = pd.DataFrame(columns=['pan', 'tilt'])
        inscovs = pd.DataFrame(columns=['Pos', 'Att', 'Vel', 'gpsweek'], dtype=object)

        with open(self.fnme, 'r+b') as fin:
            mm = mmap.mmap(fin.fileno(), 0)
            st = mm.find(DATA_START_WORD)
            gp = mm.find(GPS_START_WORD)
            gim = mm.find(GIMBAL_START_WORD)
            p_total = mm.rfind(DATA_START_WORD)
            stf = gpf = gimf = True
            if st == -1:
                stf = False
                st = np.inf
            if gp == -1:
                gpf = False
                gp = np.inf
            if gim == -1:
                gimf = False
                gim = np.inf
            # pbar = tqdm(total=p_total, file=sys.stdout, dynamic_ncols=True)
            while stf or gpf or gimf:
                next_packet = 0 if st < gp and st < gim else 1 if gp < st and gp < gim else 2 if gim < st and gim < gp \
                    else 3
                if next_packet == 0:
                    mm.seek(st + 4)  # Skip start word
                    data = mm.read(SAR_HEADER_SZ)
                    frm, syst, is_cal, att, nsam, mode = readDataHeader(data, is_slimsar=True)
                    poss_pt = mm.tell()
                    try:
                        mm.seek(nsam * 2, 1)
                        if mm.read(4) == STOP_WORD:
                            # First, find the right channel for everything
                            ch_num = -1
                            for idx, ch in enumerate(channels):
                                if mode == ch.mode or mode == ch.cal_mode:
                                    ch_num = idx
                                    break
                            if ch_num == -1:
                                # We give up, this is a weird one
                                channels.append(Channel(mode=mode, is_slimsar=True))
                            channels[ch_num].data_pts.append(poss_pt)
                            channels[ch_num].frame_num.append(frm)
                            channels[ch_num].sys_time.append(syst)
                            channels[ch_num].cals.append(is_cal)
                            channels[ch_num].atts.append(att)
                            channels[ch_num].nsam = nsam
                            # pbar.update(mm.tell() - poss_pt)
                            if mm.tell() / p_total - self.perc_complete > 0:
                                self.perc_complete += .05
                                print('|', end='')
                        else:
                            mm.seek(poss_pt)
                    except ValueError:
                        mm.seek(poss_pt)
                    st = mm.find(DATA_START_WORD)
                    stf = False if st == -1 else True
                    st = np.inf if not stf else st
                elif next_packet == 1:
                    # GPS Packet
                    mm.seek(gp)
                    data = mm.read(15)
                    ppstime = int.from_bytes(data[4:8], byteorder='big')
                    header_length = data[11]
                    packet_type = int.from_bytes(data[12:14], byteorder='little')
                    hl = header_length + 8
                    if packet_type == 42:
                        # BESTPOS packet
                        data = data + mm.read(hl + 32 - 15)
                        gpsweek = int.from_bytes(data[22:24], byteorder='little')
                        gpsms = int.from_bytes(data[24:28], byteorder='little') / 1000
                        lat = struct.unpack('<d', data[hl + 8:hl + 16])[0]
                        lon = struct.unpack('<d', data[hl + 16:hl + 24])[0]
                        alt = struct.unpack('<d', data[hl + 24:hl + 32])[0]  # + undulationEGM96(lat, lon)
                        gps_df.loc[gpsms, ['lat', 'lon', 'alt', 'systime', 'gps_wk', 'ptype']] = \
                            [lat, lon, alt, ppstime, gpsweek, 0]
                    elif packet_type == 508:
                        hl = 8 + 12
                        # INSPVAS packet
                        data = data + mm.read(hl + 84 - 15)
                        gpsweek = struct.unpack('<l', data[hl:hl + 4])[0]
                        gpsms = struct.unpack('<d', data[hl + 4:hl + 12])[0]
                        lat = struct.unpack('<d', data[hl + 12:hl + 20])[0]
                        lon = struct.unpack('<d', data[hl + 20:hl + 28])[0]
                        alt = struct.unpack('<d', data[hl + 28:hl + 36])[0]
                        vel_n = struct.unpack('<d', data[hl + 36:hl + 44])[0]
                        vel_e = struct.unpack('<d', data[hl + 44:hl + 52])[0]
                        vel_u = struct.unpack('<d', data[hl + 52:hl + 60])[0]
                        roll = struct.unpack('<d', data[hl + 60:hl + 68])[0] * DTR
                        pitch = struct.unpack('<d', data[hl + 68:hl + 76])[0] * DTR
                        yaw = struct.unpack('<d', data[hl + 76:hl + 84])[0] * DTR
                        azimuthX = np.cos(yaw)
                        azimuthY = np.sin(yaw)
                        gps_df.loc[gpsms, ['lat', 'lon', 'alt',
                                           'vn', 've', 'vu', 'r', 'p', 'y', 'azimuthX', 'azimuthY', 'systime',
                                           'gps_wk', 'ptype']] = \
                            [lat, lon, alt, vel_n, vel_e, vel_u, roll,
                             pitch, yaw, azimuthX, azimuthY, ppstime, gpsweek, 1]
                    elif packet_type == 492:
                        # TIMESYNC packet
                        data = data + mm.read(hl + 8 - 15)
                        gpsweek = struct.unpack('<l', data[hl:hl + 4])[0]
                        gpsms = struct.unpack('<l', data[hl + 4:hl + 8])[0]
                        timesync.loc[timesync_n, ['week', 'secs', 'systime']] = \
                            [gpsweek, gpsms / 1000, ppstime]
                        timesync_n += 1
                    elif packet_type == 320:
                        # INSCOVS packet
                        hl = 8 + 12
                        data = data + mm.read(hl + 228 - 15)
                        if get_cov:
                            gpsweek = struct.unpack('<l', data[hl:hl + 4])[0]
                            gpsms = struct.unpack('<d', data[hl + 4:hl + 12])[0]
                            pos_cov = np.zeros((3, 3))
                            att_cov = np.zeros((3, 3))
                            vel_cov = np.zeros((3, 3))
                            for x in range(3):
                                for y in range(3):
                                    pos_cov[x, y] = \
                                        struct.unpack('<d',
                                                      data[hl + 12 + 8 * x + 24 * y:hl + 12 + 8 * x + 24 * y + 8])[0]
                            for x in range(3):
                                for y in range(3):
                                    att_cov[x, y] = \
                                        struct.unpack('<d',
                                                      data[hl + 84 + 8 * x + 24 * y:hl + 84 + 8 * x + 24 * y + 8])[0]
                            for x in range(3):
                                for y in range(3):
                                    vel_cov[x, y] = \
                                        struct.unpack('<d',
                                                      data[hl + 156 + 8 * x + 24 * y:hl + 156 + 8 * x + 24 * y + 8])[0]
                            inscovs.loc[gpsms, ['Pos', 'Att', 'Vel', 'gpsweek']] = [pos_cov, att_cov, vel_cov, gpsweek]
                    mm.seek(gp + 4)
                    gp = mm.find(GPS_START_WORD)
                    gpf = False if gp == -1 else True
                    gp = np.inf if not gpf else gp
                elif next_packet == 2:
                    # Gimbal packet
                    mm.seek(gim)
                    data = mm.read(16)
                    syst = int.from_bytes(data[4:8], byteorder='big')
                    pan = int.from_bytes(data[8:10], byteorder='big', signed=True) * P_CONV * DTR
                    tilt = int.from_bytes(data[10:12], byteorder='big', signed=True) * T_CONV * DTR
                    gimbal_df.loc[syst, ['pan', 'tilt']] = [pan, tilt]
                    gim = mm.find(GIMBAL_START_WORD)
                    gimf = False if gim == -1 else True
                    gim = np.inf if not gimf else gim
            pbar.close()

        # Final GPS frame calculations
        gps_df['frames'] = gps_df.shape[0]

        # INSPVA systime jump corrections
        jumps = 0
        prev = 0
        for idx, row in gps_df.iterrows():
            if row['systime'] < prev:
                jumps += 1
            prev = row['systime']
            gps_df.loc[idx, 'corr_systime'] = row['systime'] + INT_MAX * jumps
        gps_df['systime'] = gps_df['corr_systime']
        gps_df = gps_df.drop(columns='corr_systime').astype(np.float64)
        gps_df['frames'] = gps_df.shape[0]

        # Jump correction for TIMESYNC systime
        jumps = 0
        prev = 0
        for idx, row in timesync.iterrows():
            if row['systime'] < prev:
                jumps += 1
            prev = row['systime']
            timesync.loc[idx, 'corr_systime'] = row['systime'] + INT_MAX * jumps
        timesync['systime'] = timesync['corr_systime']
        timesync = timesync.drop(columns='corr_systime').astype(np.float64)

        # Interpolate to correct systime points
        try:
            tac_per_sec = np.diff(timesync['systime'].values).mean()
            timesync_secs = np.concatenate(([timesync.loc[0, 'secs'] - 1], timesync['secs'].values,
                                            [timesync.loc[timesync.shape[0] - 1, 'secs'] + 1]))
            timesync_tac = np.concatenate(([timesync.loc[0, 'systime'] - tac_per_sec], timesync['systime'].values,
                                           [timesync.loc[timesync.shape[0] - 1, 'systime'] + tac_per_sec]))
            gps_df['systime'] = np.interp(gps_df.index.values, timesync_secs, timesync_tac)
            bestpos = gps_df.loc[gps_df['ptype'] == 0].dropna(axis=1)
            gps_df = gps_df.loc[gps_df['ptype'] == 1]
            # gps_df = gps_df.sort_index().dropna()
            gps_df['lat'] = jumpCorrection(gps_df['lat'].values)
            gps_df['lon'] = jumpCorrection(gps_df['lon'].values)
            gps_df['alt'] = jumpCorrection(gps_df['alt'].values)
        except KeyError:
            bestpos = None
            print('No GPS data found.')

        # Final gimbal frame calcs
        gimbal_df = gimbal_df.sort_index().reset_index()
        gimbal_df = gimbal_df.rename(columns={'index': 'systime'})

        # Final INSCOVS frame calcs
        self.inscovs = inscovs

        self.gimbal = gimbal_df
        self.gps_data = gps_df
        self.bestpos = bestpos
        self.timesync = timesync
        for ch in channels:
            ch.unwrapSystemTime()

            # Remove cal data
            ch.cals = np.array(ch.cals)
            if sum(ch.cals) != 0:
                cal_fin = np.max(np.array(ch.frame_num)[ch.cals])
                ch.cal_num = np.array(ch.frame_num)[:cal_fin]
                ch.cal_pts = np.array(ch.data_pts)[:cal_fin]
            else:
                cal_fin = 0
            ch.ncals = cal_fin
            ch.frame_num = np.array(ch.frame_num[cal_fin:]) - cal_fin
            ch.packet_points = np.array(ch.data_pts)[cal_fin:]
            ch.sys_time = np.array(ch.sys_time[cal_fin:])
            ch.atts = np.array(ch.atts[cal_fin:])
            ch.nframes = len(ch.frame_num)
            try:
                ch.pulse_length = int(ch.xml['Pulse_Length_S'] * ch.fs)
            except TypeError:
                # The channel was added during parsing
                ch.pulse_length = ch.nsam

        self.channels = channels
        # Now that channels are defined we can get a reference chirp
        for ch_idx, ch in enumerate(channels):
            if sum(ch.cals) == 0:
                ch.ref_chirp = None
            else:
                ch.ref_chirp = np.mean(self.getPulses(ch.cal_num, ch_idx, is_cal=True), axis=1)
            ch.matched_filter = None

    def __getitem__(self, n):
        return self.channels[n]

    def __len__(self):
        return len(self.channels)

    def loadGPS(self, new_gps_data):
        """
        Changes out GPS data.
        :param new_gps_data: DataFrame of GPS data, same format as original.
        :return: Nothing.
        """
        self.gps_data = new_gps_data

    def loadASH(self, fnme=None):
        """
        Loads the .ASH file into the object.
        :param fnme: Path to .ASH file.
        :return: saved .ASH data, as a dict.
        """
        if fnme is not None:
            self.ash = loadASHFile(fnme)
        return self.ash

    def loadASI(self, fnme=None):
        """
        Loads .asi file data, using previously acquired .ash data.
        :param fnme: Path to .asi file.
        :return: Numpy array of complex .asi file data.
        """
        if fnme is None:
            if self.pol is None:
                self.asi = loadASIFile(self.files['asi'],
                                       self.ash['image']['nRows'],
                                       self.ash['image']['nCols'])
            else:
                self.asi = loadASIFile(self.files['asi'][self.pol],
                                       self.ash['image']['nRows'],
                                       self.ash['image']['nCols'])
        else:
            self.asi = loadASIFile(fnme,
                                   self.ash['image']['nRows'],
                                   self.ash['image']['nCols'])
        return self.asi

    def genMatchedFilter(self, channel=0, pick=None):
        """
        Generates a matched filter using saved calibration data.
        :param channel: int Channel number to generate filter for.
        :param pick: Pickle file of matched filter data. If None, uses calibration data
            and generates one.
        :return: Nothing.
        """
        ch = self.channels[channel]
        if pick is not None:
            ch.matched_filter = pickle.load(open(pick, 'rb'))['mf']
        else:
            # First, get the actual pulse area from the ref chirp
            tmp = np.convolve(abs(ch.ref_chirp), np.ones((ch.pulse_length,)), mode='same')
            start = int(np.arange(ch.nsam)[tmp == tmp.max()][0] - ch.pulse_length / 2)
            stop = int(start + ch.pulse_length)
            fft_len = findPowerOf2(ch.nsam + ch.pulse_length)
            half_win_sz = int(np.ceil(ch.bw / 2 / ch.fs * fft_len))

            # Remove noise from outside actual chirp
            mf = ch.ref_chirp + 0.0
            mf[:start] = 0.0
            mf[stop:] = 0.0

            # Apply taylor window to spectrum
            tay = taylor(int(ch.bw / ch.fs * fft_len), nbar=6, sll=30)
            # tay = np.ones(int(self.xml['Bandwidth_Hz'] / fs * fft_len))
            mf = np.fft.fft(ch.ref_chirp, n=fft_len)
            mf[:half_win_sz] = mf[:half_win_sz] * tay[-half_win_sz:]
            mf[-(len(tay) - half_win_sz):] = mf[-(len(tay) - half_win_sz):] * tay[:(len(tay) - half_win_sz)]
            mf[half_win_sz:-(len(tay) - half_win_sz)] = 0.0
            ch.matched_filter = mf.conj().T

    def getPulse(self, pulse_num, channel=0, is_cal=False):
        """
        Gets pulse data from file.
        :param pulse_num: Number of pulse wanted.
        :param channel: int Channel number of pulses wanted.
        :param is_cal: If True, gets the calibration pulse of number {pulse_num}. Otherwise, gets the
            data pulse.
        :return: pulse data as a complex numpy array.
        """
        ch = self.channels[channel]
        ch2 = None
        if self.channels[channel].is_dechirp:
            # Just select the right channels for them
            if self.channels[channel].is_inphase:
                ch = self.channels[channel]
                for c in self.channels:
                    if not c.is_inphase:
                        if c.mode == ch.mode - 4096:
                            ch2 = c
                            break
            else:
                ch2 = self.channels[channel]
                for c in self.channels:
                    if not c.is_inphase:
                        if c.mode == ch2.mode + 4096:
                            ch = c
                            break
        pp = ch.packet_points[pulse_num - 1]
        pp2 = ch2.packet_points[pulse_num - 1] if ch.is_dechirp else None
        if is_cal:
            pp = ch.cal_pts[pulse_num - 1]
            pp2 = ch2.cal_pts[pulse_num - 1] if ch.is_dechirp else None
        with open(self.fnme, 'r+b') as fin:
            mm = mmap.mmap(fin.fileno(), 0)
            if ch.is_dechirp:
                ndata = np.zeros((ch.nsam,), dtype=np.complex64)
                mm.seek(pp)
                data = mm.read(2 * ch.nsam)
                tmp_data = array.array('h', data)
                tmp_data.byteswap()
                ndata += np.array(tmp_data) * 10 ** (ch.atts[pulse_num - 1] / 20)
                mm.seek(pp2)
                data = mm.read(2 * ch.nsam)
                tmp_data = array.array('h', data)
                tmp_data.byteswap()
                ndata += 1j * np.array(tmp_data) * 10 ** (ch2.atts[pulse_num - 1] / 20)
            else:
                mm.seek(pp)
                data = mm.read(2 * ch.nsam)
                tmp_data = array.array('h', data)
                tmp_data.byteswap()
                tmp_data = np.array(tmp_data)
                ndata = tmp_data * (
                        10 ** (ch.atts[pulse_num - 1] / 20))
        return ndata

    def getPulses(self, pulse_nums, channel=0, is_cal=False):
        """
        Gets pulse data from file.
        :param pulse_nums: Number of pulses wanted.
        :param channel: int Channel number of pulses wanted.
        :param is_cal: If True, gets the calibration pulse of number {pulse_num}. Otherwise, gets the
            data pulse.
        :return: pulse data as a complex numpy array.
        """
        ch = self.channels[channel]
        ch2 = None
        if self.channels[channel].is_dechirp:
            # Just select the right channels for them
            if self.channels[channel].is_inphase:
                ch = self.channels[channel]
                for c in self.channels:
                    if not c.is_inphase:
                        if c.mode == ch.mode - 4096:
                            ch2 = c
                            break
            else:
                ch2 = self.channels[channel]
                for c in self.channels:
                    if c.is_inphase:
                        if c.mode == ch2.mode + 4096:
                            ch = c
                            break
        if ch.is_dechirp:
            pdata = np.zeros((ch.nsam, len(pulse_nums)), dtype=np.complex128)
        else:
            pdata = np.zeros((ch.nsam, len(pulse_nums)))
        with open(self.fnme, 'r+b') as fin:
            mm = mmap.mmap(fin.fileno(), 0)
            for idx, p in enumerate(pulse_nums):
                try:
                    if is_cal:
                        pp = ch.cal_pts[p - 1]
                        pp2 = ch2.cal_pts[p - 1] if ch.is_dechirp else None
                        att = 31
                        att2 = 31
                    else:
                        pp = ch.packet_points[p - 1]
                        pp2 = ch2.packet_points[p - 1] if ch.is_dechirp else None
                        att = ch.atts[p - 1]
                        att2 = ch2.atts[p - 1]
                except IndexError:
                    print('IDX is {} and p is {}'.format(idx, p))
                mm.seek(pp)
                data = mm.read(2 * ch.nsam)
                tmp_data = array.array('h', data)
                tmp_data.byteswap()
                tmp_data = np.array(tmp_data)
                ndata = (tmp_data * (10 ** (att / 20)))
                if ch.is_dechirp:
                    ndata = (tmp_data * (10 ** (att / 20))).astype(np.complex64)
                    mm.seek(pp2)
                    data = mm.read(2 * ch2.nsam)
                    tmp_data = array.array('h', data)
                    tmp_data.byteswap()
                    tmp_data = np.array(tmp_data)
                    ndata += 1j * tmp_data * (10 ** (att2 / 20))
                pdata[:, idx] = ndata
        return pdata

    def getPulseGen(self, chunk_sz=64, channel=0):
        """
        This is a generator function version of getPulse, above.
        :param channel: int Number of channel to get iterator for.
        :param chunk_sz: Size of each chunk returned by the generator.
        :return: Generator with
            pdata - pulse data as a numpy array.
            pulse_nums - list of pulse numbers.
            atts - attenuation factors of pulses returned.
            sys_time - system times, in TAC, of pulses returned.
        """
        chan = self.channels[channel]
        chan2 = None
        if self.channels[channel].is_dechirp:
            # Just select the right channels for them
            if self.channels[channel].is_inphase:
                chan = self.channels[channel]
                for c in self.channels:
                    if not c.is_inphase:
                        if c.mode == chan.mode - 4096:
                            chan2 = c
                            break
            else:
                chan2 = self.channels[channel]
                for c in self.channels:
                    if not c.is_inphase:
                        if c.mode == chan2.mode + 4096:
                            chan = c
                            break
        nsam = chan.nsam
        with open(self.fnme, 'r+b') as fin:
            mm = mmap.mmap(fin.fileno(), 0)
            for ch in np.arange(0, chan.nframes, chunk_sz):
                pulse_nums = np.arange(ch, min(ch + chunk_sz, chan.nframes))
                if chan.is_dechirp:
                    pdata = np.zeros((nsam, len(pulse_nums)), dtype=np.complex128)
                else:
                    pdata = np.zeros((nsam, len(pulse_nums)))
                for idx, p in enumerate(pulse_nums):
                    try:
                        pp = chan.packet_points[p]
                    except IndexError:
                        print('IDX is {} and p is {}'.format(idx, p))
                        pp = chan.packet_points[p]
                    mm.seek(pp)
                    data = mm.read(2 * nsam)
                    tmp_data = array.array('h', data)
                    tmp_data.byteswap()
                    tmp_data = np.array(tmp_data)
                    ndata = tmp_data * (10 ** (chan.atts[p] / 20))
                    if chan.is_dechirp:
                        ndata = (tmp_data * (10 ** (chan.atts[p] / 20))).astype(np.complex64)
                        try:
                            pp = chan2.packet_points[p]
                        except IndexError:
                            print('IDX is {} and p is {}'.format(idx, p))
                            pp = chan2.packet_points[p]
                        mm.seek(pp)
                        data = mm.read(2 * nsam)
                        tmp_data = array.array('h', data)
                        tmp_data.byteswap()
                        tmp_data = np.array(tmp_data)
                        ndata += 1j * (tmp_data * (10 ** (chan.atts[p] / 20))).astype(np.complex64)
                    pdata[:, idx] = ndata
                yield pdata, pulse_nums, chan.atts[pulse_nums], chan.sys_time[pulse_nums]

    def buildSAR(self, output_fnme):
        """
        Experimental SAR builder function. Not finished, don't use it.
        :param output_fnme:
        :return:
        """

        output_sar_fnme = output_fnme + '.sar'
        output_xml_fnme = output_fnme + '.xml'
        if not exists(output_sar_fnme):
            copy(self.fnme, output_sar_fnme)
        if not exists(output_xml_fnme):
            copy(self.files['xml'], output_xml_fnme)

        # gpsd = self.gps_data if gps_data is None else gps_data

        with open(output_sar_fnme, 'r+b') as fout:
            mm = mmap.mmap(fout.fileno(), 0)
            for pulse in self.packet_points['gps']:
                mm.seek(pulse)
                data = mm.read(37)
                header_length = data[11]
                packet_type = int.from_bytes(data[12:14], byteorder='little')
                if packet_type == 42:
                    gpsms = int.from_bytes(data[24:28], byteorder='little') / 1000
                    gd = self.gps_data.loc[gpsms]
                    hl = header_length + 8
                    mm.seek(pulse + hl + 8)
                    mm.write(struct.pack('<d', gd['lat']))  # lat
                    mm.write(struct.pack('<d', gd['lon']))  # lon
                    mm.write(struct.pack('<d', gd['alt']))  # alt
                elif packet_type == 508:
                    gpsms = struct.unpack('<d', data[24:32])[0]
                    gd = self.gps_data.loc[gpsms]
                    hl = 8 + 12
                    mm.seek(pulse + hl + 12)
                    mm.write(struct.pack('<d', gd['lat']))  # lat
                    mm.write(struct.pack('<d', gd['lon']))  # lon
                    mm.write(struct.pack('<d', gd['alt']))  # alt
                    mm.write(struct.pack('<d', gd['vn']))  # vn
                    mm.write(struct.pack('<d', gd['ve']))  # ve
                    mm.write(struct.pack('<d', gd['vu']))  # vu
                    mm.write(struct.pack('<d', gd['r'] * 180 / np.pi))  # roll in deg
                    mm.write(struct.pack('<d', gd['p'] * 180 / np.pi))  # pitch in deg
                    mm.write(struct.pack('<d', gd['y'] * 180 / np.pi))  # yaw in deg


class SDRParse(object):

    def __init__(self, fnme=None, debug_dir='/home/jeff/repo/Debug', pol=None, nframes=None):
        # Get the file we'll be parsing, and some extra data for it
        self.fnme = fnme
        self.files = findAllFilenames(fnme, debug_dir)
        self.asi = None
        self.xml = loadXMLFile(self.files['xml'], True)['SlimSDR_Configuration']['SlimSDR_Info']
        self.n_channels = len([1 for ch in self.xml if 'Channel_' in ch]) - 1
        self.n_ants = len([1 for ch in self.xml['Common_Channel_Settings']['Antenna_Settings'] if 'Antenna_' in ch])
        self.perc_complete = 0
        channels = []

        # Get channel specific data for each channel
        for channel in range(self.n_channels):
            ch_settings = self.xml['Channel_{}'.format(channel)]
            channels.append(Channel(ch_settings))
            if 'Simultaneous_Receive_Modes' in ch_settings:
                # Just slap another channel in here
                channels.append(Channel(ch_settings))
            self.pol = pol

        # There is some extra data in the .ash file that may be wanted
        # It's not necessary, though, so just check for it
        if 'ash' in self.files:
            if pol is not None:
                try:
                    self.ash = loadASHFile(self.files['ash'][pol])
                except IndexError:
                    print('No ' + pol + ' ASH file found')
            else:
                try:
                    self.ash = loadASHFile(self.files['ash'])
                except IndexError:
                    try:
                        self.ash = loadASHFile(self.files['ash'][list(self.files['ash'].keys())[0]])
                    except IndexError:
                        print('No ASH file found')

        try:
            fl_len = np.sqrt(
                (self.xml['Flight_Line']['Start_Latitude_D'] - self.xml['Flight_Line']['Stop_Latitude_D']) ** 2 +
                (self.xml['Flight_Line']['Start_Longitude_D'] - self.xml['Flight_Line']['Stop_Longitude_D']) ** 2)
            self.flight_len = fl_len / (self.xml['Common_Channel_Settings']['Velocity_Knots'] * .514444)
        except KeyError:
            self.flight_len = 4

        timesync_n = 0
        gps_df = pd.DataFrame(columns=['lat', 'lon', 'alt',
                                       'vn', 've', 'vu', 'r', 'p', 'y', 'azimuthX', 'azimuthY', 'systime',
                                       'corr_systime', 'gps_wk', 'ptype'])
        timesync = pd.DataFrame(columns=['week', 'secs', 'systime', 'corr_systime'])
        gimbal_df = pd.DataFrame(columns=['pan', 'tilt'])
        inscovs = pd.DataFrame(columns=['Pos', 'Att', 'Vel', 'gpsweek'], dtype=object)

        with open(self.fnme, 'r+b') as fin:
            mm = mmap.mmap(fin.fileno(), 0)
            st = mm.find(DATA_START_WORD)
            gp = mm.find(GPS_START_WORD)
            gim = mm.find(GIMBAL_START_WORD)
            stf = gpf = gimf = True
            if st == -1:
                stf = False
            if gp == -1:
                gpf = False
            if gim == -1:
                gimf = False
            pbar_max = mm.rfind(DATA_START_WORD)
            # pbar = tqdm(total=pbar_max, file=sys.stdout, dynamic_ncols=True)
            while stf and gpf and gimf:
                next_packet = 0 if st < gp and st < gim else 1 if gp < st and gp < gim else 2 if gim < st and gim < gp \
                    else 3
                if next_packet == 0:
                    mm.seek(st + 4)  # Skip start word
                    data = mm.read(SDR_HEADER_SZ)
                    frm, syst, is_cal, att, nsam, channel = readDataHeader(data)
                    poss_pt = mm.tell()
                    try:
                        mm.seek(nsam * 4, 1)
                        if mm.read(4) == STOP_WORD:
                            try:
                                channels[channel].data_pts.append(poss_pt)
                                channels[channel].frame_num.append(frm)
                                channels[channel].sys_time.append(syst)
                                channels[channel].cals.append(is_cal)
                                channels[channel].atts.append(att)
                                channels[channel].nsam = nsam
                            except IndexError:
                                print('Channel {} not found.'.format(channel))
                            # pbar.update(mm.tell() - poss_pt)
                            if mm.tell() / pbar_max - self.perc_complete > 0:
                                self.perc_complete += .05
                                print('|', end='')
                            if nframes is not None:
                                em_stop = True
                                for ch in channels:
                                    if len(ch.frame_num) == 0:
                                        em_stop = False
                                    elif ch.frame_num[-1] < nframes:
                                        em_stop = False
                                if em_stop:
                                    break
                        else:
                            mm.seek(poss_pt)
                    except ValueError:
                        mm.seek(poss_pt)
                    st = mm.find(DATA_START_WORD)
                    stf = False if st == -1 else True
                    st = np.inf if not stf else st
                elif next_packet == 1:
                    # GPS Packet
                    mm.seek(gp)
                    data = mm.read(15)
                    ppstime = int.from_bytes(data[4:8], byteorder='big')
                    header_length = data[11]
                    packet_type = int.from_bytes(data[12:14], byteorder='little')
                    hl = header_length + 8
                    if packet_type == 42:
                        # BESTPOS packet
                        data = data + mm.read(hl + 32 - 15)
                        gpsweek = int.from_bytes(data[22:24], byteorder='little')
                        gpsms = int.from_bytes(data[24:28], byteorder='little') / 1000
                        lat = struct.unpack('<d', data[hl + 8:hl + 16])[0]
                        lon = struct.unpack('<d', data[hl + 16:hl + 24])[0]
                        alt = struct.unpack('<d', data[hl + 24:hl + 32])[0]  # + undulationEGM96(lat, lon)
                        gps_df.loc[gpsms, ['lat', 'lon', 'alt', 'systime', 'gps_wk', 'ptype']] = \
                            [lat, lon, alt, ppstime, gpsweek, 0]
                    elif packet_type == 508:
                        hl = 8 + 12
                        # INSPVAS packet
                        data = data + mm.read(hl + 84 - 15)
                        gpsweek = struct.unpack('<l', data[hl:hl + 4])[0]
                        gpsms = struct.unpack('<d', data[hl + 4:hl + 12])[0]
                        lat = struct.unpack('<d', data[hl + 12:hl + 20])[0]
                        lon = struct.unpack('<d', data[hl + 20:hl + 28])[0]
                        alt = struct.unpack('<d', data[hl + 28:hl + 36])[0]
                        vel_n = struct.unpack('<d', data[hl + 36:hl + 44])[0]
                        vel_e = struct.unpack('<d', data[hl + 44:hl + 52])[0]
                        vel_u = struct.unpack('<d', data[hl + 52:hl + 60])[0]
                        roll = struct.unpack('<d', data[hl + 60:hl + 68])[0] * DTR
                        pitch = struct.unpack('<d', data[hl + 68:hl + 76])[0] * DTR
                        yaw = struct.unpack('<d', data[hl + 76:hl + 84])[0] * DTR
                        azimuthX = np.cos(yaw)
                        azimuthY = np.sin(yaw)
                        gps_df.loc[gpsms, ['lat', 'lon', 'alt',
                                           'vn', 've', 'vu', 'r', 'p', 'y', 'azimuthX', 'azimuthY', 'systime',
                                           'gps_wk', 'ptype']] = \
                            [lat, lon, alt, vel_n, vel_e, vel_u, roll,
                             pitch, yaw, azimuthX, azimuthY, ppstime, gpsweek, 1]
                    elif packet_type == 492:
                        # TIMESYNC packet
                        data = data + mm.read(hl + 8 - 15)
                        gpsweek = struct.unpack('<l', data[hl:hl + 4])[0]
                        gpsms = struct.unpack('<l', data[hl + 4:hl + 8])[0]
                        timesync.loc[timesync_n, ['week', 'secs', 'systime']] = \
                            [gpsweek, gpsms / 1000, ppstime]
                        timesync_n += 1
                    elif packet_type == 320:
                        # INSCOVS packet
                        hl = 8 + 12
                        data = data + mm.read(hl + 228 - 15)
                        gpsweek = struct.unpack('<l', data[hl:hl + 4])[0]
                        gpsms = struct.unpack('<d', data[hl + 4:hl + 12])[0]
                        pos_cov = np.zeros((3, 3))
                        att_cov = np.zeros((3, 3))
                        vel_cov = np.zeros((3, 3))
                        for x in range(3):
                            for y in range(3):
                                pos_cov[x, y] = \
                                    struct.unpack('<d', data[hl + 12 + 8 * x + 24 * y:hl + 12 + 8 * x + 24 * y + 8])[0]
                        for x in range(3):
                            for y in range(3):
                                att_cov[x, y] = \
                                    struct.unpack('<d', data[hl + 84 + 8 * x + 24 * y:hl + 84 + 8 * x + 24 * y + 8])[0]
                        for x in range(3):
                            for y in range(3):
                                vel_cov[x, y] = \
                                    struct.unpack('<d', data[hl + 156 + 8 * x + 24 * y:hl + 156 + 8 * x + 24 * y + 8])[
                                        0]
                        inscovs.loc[gpsms, ['Pos', 'Att', 'Vel', 'gpsweek']] = [pos_cov, att_cov, vel_cov, gpsweek]
                    mm.seek(gp + 4)
                    gp = mm.find(GPS_START_WORD)
                    gpf = False if gp == -1 else True
                    gp = np.inf if not gpf else gp
                elif next_packet == 2:
                    # Gimbal packet
                    mm.seek(gim)
                    data = mm.read(16)
                    syst = int.from_bytes(data[4:8], byteorder='big')
                    pan = int.from_bytes(data[8:10], byteorder='big', signed=True) * P_CONV * DTR
                    tilt = int.from_bytes(data[10:12], byteorder='big', signed=True) * T_CONV * DTR
                    gimbal_df.loc[syst, ['pan', 'tilt']] = [pan, tilt]
                    gim = mm.find(GIMBAL_START_WORD)
                    gimf = False if gim == -1 else True
                    gim = np.inf if not gimf else gim
            # pbar.close()

        # Final GPS frame calculations
        gps_df['frames'] = gps_df.shape[0]

        # INSPVA systime jump corrections
        jumps = 0
        prev = 0
        for idx, row in gps_df.iterrows():
            if row['systime'] < prev:
                jumps += 1
            prev = row['systime']
            gps_df.loc[idx, 'corr_systime'] = row['systime'] + INT_MAX * jumps
        gps_df['systime'] = gps_df['corr_systime']
        gps_df = gps_df.drop(columns='corr_systime').astype(np.float64)
        gps_df['frames'] = gps_df.shape[0]

        # Jump correction for TIMESYNC systime
        jumps = 0
        prev = 0
        for idx, row in timesync.iterrows():
            if row['systime'] < prev:
                jumps += 1
            prev = row['systime']
            timesync.loc[idx, 'corr_systime'] = row['systime'] + INT_MAX * jumps
        timesync['systime'] = timesync['corr_systime']
        timesync = timesync.drop(columns='corr_systime').astype(np.float64)

        # Interpolate to correct systime points
        try:
            tac_per_sec = np.diff(timesync['systime'].values).mean()
            timesync_secs = np.concatenate(([timesync.loc[0, 'secs'] - 1], timesync['secs'].values,
                                            [timesync.loc[timesync.shape[0] - 1, 'secs'] + 1]))
            timesync_tac = np.concatenate(([timesync.loc[0, 'systime'] - tac_per_sec], timesync['systime'].values,
                                           [timesync.loc[timesync.shape[0] - 1, 'systime'] + tac_per_sec]))
            gps_df['systime'] = np.interp(gps_df.index.values, timesync_secs, timesync_tac)
            bestpos = gps_df.loc[gps_df['ptype'] == 0].dropna(axis=1)
            gps_df = gps_df.loc[gps_df['ptype'] == 1]
            # gps_df = gps_df.sort_index().dropna()
            gps_df['lat'] = jumpCorrection(gps_df['lat'].values)
            gps_df['lon'] = jumpCorrection(gps_df['lon'].values)
            gps_df['alt'] = jumpCorrection(gps_df['alt'].values)
        except KeyError:
            bestpos = None
            print('No GPS data found.')

        # Final gimbal frame calcs
        if gimbal_df.shape[0] == 0:
            gimbal_df = None
            print('No gimbal data found')
        else:
            gimbal_df = gimbal_df.sort_index().reset_index()
            gimbal_df = gimbal_df.rename(columns={'index': 'systime'})

        # Final INSCOVS frame calcs
        if inscovs.shape[0] == 0:
            self.inscovs = None
            print('No INSCOVS data found')
        else:
            self.inscovs = inscovs

        self.gimbal = gimbal_df
        self.gps_data = gps_df
        self.bestpos = bestpos
        self.timesync = timesync
        for ch in channels:
            ch.unwrapSystemTime()

            # Remove cal data
            ch.cals = np.array(ch.cals)
            cal_fin = np.max(np.array(ch.frame_num)[ch.cals])
            ch.ncals = cal_fin
            ch.cal_num = np.array(ch.frame_num)[:cal_fin]
            ch.cal_pts = np.array(ch.data_pts)[:cal_fin]
            ch.frame_num = np.array(ch.frame_num[cal_fin:]) - cal_fin
            ch.packet_points = np.array(ch.data_pts)[cal_fin:]
            ch.sys_time = np.array(ch.sys_time[cal_fin:])
            ch.atts = np.array(ch.atts[cal_fin:])
            ch.nframes = len(ch.frame_num)
            ch.pulse_length = int(ch.xml['Pulse_Length_S'] * ch.fs)

        self.channels = channels
        # Now that channels are defined we can get a reference chirp
        for ch_idx, ch in enumerate(channels):
            ch.ref_chirp = np.mean(self.getPulses(ch.cal_num, ch_idx, is_cal=True), axis=1)
            ch.matched_filter = None

    def __getitem__(self, n):
        return self.channels[n]

    def __len__(self):
        return len(self.channels)

    def loadGPS(self, new_gps_data):
        """
        Changes out GPS data.
        :param new_gps_data: DataFrame of GPS data, same format as original.
        :return: Nothing.
        """
        self.gps_data = new_gps_data

    def loadASH(self, fnme=None):
        """
        Loads the .ASH file into the object.
        :param fnme: Path to .ASH file.
        :return: saved .ASH data, as a dict.
        """
        if fnme is not None:
            self.ash = loadASHFile(fnme)
        return self.ash

    def loadASI(self, fnme=None):
        """
        Loads .asi file data, using previously acquired .ash data.
        :param fnme: Path to .asi file.
        :return: Numpy array of complex .asi file data.
        """
        if fnme is None:
            if self.pol is None:
                self.asi = loadASIFile(self.files['asi'],
                                       self.ash['image']['nRows'],
                                       self.ash['image']['nCols'])
            else:
                self.asi = loadASIFile(self.files['asi'][self.pol],
                                       self.ash['image']['nRows'],
                                       self.ash['image']['nCols'])
        else:
            self.asi = loadASIFile(fnme,
                                   self.ash['image']['nRows'],
                                   self.ash['image']['nCols'])
        return self.asi

    def genMatchedFilter(self, channel=0, pick=None):
        """
        Generates a matched filter using saved calibration data.
        :param channel: int Channel number to generate filter for.
        :param pick: Pickle file of matched filter data. If None, uses calibration data
            and generates one.
        :return: Nothing.
        """
        ch = self.channels[channel]
        if pick is not None:
            ch.matched_filter = pickle.load(open(pick, 'rb'))['mf']
        else:
            # First, get the actual pulse area from the ref chirp
            tmp = np.convolve(abs(ch.ref_chirp), np.ones((ch.pulse_length,)), mode='same')
            start = int(np.arange(ch.nsam)[tmp == tmp.max()][0] - ch.pulse_length / 2)
            stop = int(start + ch.pulse_length)
            fft_len = findPowerOf2(ch.nsam + ch.pulse_length)
            half_win_sz = int(np.ceil(ch.xml['Bandwidth_Hz'] / 2 / ch.r_fs * fft_len))

            # Remove noise from outside actual chirp
            mf = ch.ref_chirp + 0.0
            mf[:start] = 0.0
            mf[stop:] = 0.0

            # Apply taylor window to spectrum
            tay = taylor(int(ch.xml['Bandwidth_Hz'] / ch.r_fs * fft_len), nbar=6, sll=30)
            # tay = np.ones(int(self.xml['Bandwidth_Hz'] / fs * fft_len))
            mf = np.fft.fft(ch.ref_chirp, n=fft_len)
            mf[:half_win_sz] = mf[:half_win_sz] * tay[-half_win_sz:]
            mf[-(len(tay) - half_win_sz):] = mf[-(len(tay) - half_win_sz):] * tay[:(len(tay) - half_win_sz)]
            mf[half_win_sz:-(len(tay) - half_win_sz)] = 0.0
            ch.matched_filter = mf.conj().T

    def getPulse(self, pulse_num, channel=0, is_cal=False):
        """
        Gets pulse data from file.
        :param pulse_num: Number of pulse wanted.
        :param channel: int Channel number of pulses wanted.
        :param is_cal: If True, gets the calibration pulse of number {pulse_num}. Otherwise, gets the
            data pulse.
        :return: pulse data as a complex numpy array.
        """
        ch = self.channels[channel]
        pp = ch.packet_points[pulse_num - 1]

        if is_cal:
            pp = ch.cal_pts[pulse_num - 1]
            atts = 31
        else:
            atts = ch.atts[pulse_num - 1]
        with open(self.fnme, 'r+b') as fin:
            mm = mmap.mmap(fin.fileno(), 0)
            mm.seek(pp)
            data = mm.read(4 * ch.nsam)
            tmp_data = array.array('h', data)
            tmp_data.byteswap()
            tmp_data = np.array(tmp_data)
            ndata = (tmp_data[0:ch.nsam * 2:2] + 1j * tmp_data[1:ch.nsam * 2:2]) * (
                    10 ** (atts / 20))
        return ndata

    def getPulses(self, pulse_nums, channel=0, is_cal=False):
        """
        Gets pulse data from file.
        :param pulse_nums: Number of pulses wanted.
        :param channel: int Channel number of pulses wanted.
        :param is_cal: If True, gets the calibration pulse of number {pulse_num}. Otherwise, gets the
            data pulse.
        :return: pulse data as a complex numpy array.
        """
        ch = self.channels[channel]
        pdata = np.zeros((ch.nsam, len(pulse_nums)), dtype=np.complex128)
        with open(self.fnme, 'r+b') as fin:
            mm = mmap.mmap(fin.fileno(), 0)
            for idx, p in enumerate(pulse_nums):
                try:
                    if is_cal:
                        pp = ch.cal_pts[p - 1]
                        atts = 31
                    else:
                        pp = ch.packet_points[p - 1]
                        atts = ch.atts[p - 1]
                except IndexError:
                    print('IDX is {} and p is {}'.format(idx, p))
                mm.seek(pp)
                data = mm.read(4 * ch.nsam)
                tmp_data = array.array('h', data)
                tmp_data.byteswap()
                tmp_data = np.array(tmp_data)
                ndata = (tmp_data[0:ch.nsam * 2:2] + 1j * tmp_data[1:ch.nsam * 2:2]) * (
                        10 ** (atts / 20))
                pdata[:, idx] = ndata
        return pdata

    def getPulseGen(self, chunk_sz=64, channel=0):
        """
        This is a generator function version of getPulse, above.
        :param channel: int Number of channel to get iterator for.
        :param chunk_sz: Size of each chunk returned by the generator.
        :return: Generator with
            pdata - pulse data as a numpy array.
            pulse_nums - list of pulse numbers.
            atts - attenuation factors of pulses returned.
            sys_time - system times, in TAC, of pulses returned.
        """
        chan = self.channels[channel]
        nsam = chan.nsam
        with open(self.fnme, 'r+b') as fin:
            mm = mmap.mmap(fin.fileno(), 0)
            for ch in np.arange(0, chan.nframes, chunk_sz):
                pulse_nums = np.arange(ch, min(ch + chunk_sz, chan.nframes))
                pdata = np.zeros((nsam, len(pulse_nums)), dtype=np.complex128)
                for idx, p in enumerate(pulse_nums):
                    try:
                        pp = chan.packet_points[p]
                    except IndexError:
                        print('IDX is {} and p is {}'.format(idx, p))
                        pp = chan.packet_points[p]
                    mm.seek(pp)
                    data = mm.read(4 * nsam)
                    tmp_data = array.array('h', data)
                    tmp_data.byteswap()
                    tmp_data = np.array(tmp_data)
                    ndata = (tmp_data[0:nsam * 2:2] + 1j * tmp_data[1:nsam * 2:2]) * (10 ** (chan.atts[p] / 20))
                    pdata[:, idx] = ndata
                yield pdata, pulse_nums, chan.atts[pulse_nums], chan.sys_time[pulse_nums]


def jumpCorrection(a_data):
    """Performs jump correction on the GPS data. The Kalman filter that
    corrects the GPS solution also introduces jumps into the data. On newer
    INSs, the jumps are smoothed out. One of the systems we use has an
    older INS, so we need to smooth out the jumps ourselves. The function
    takes one set of data, so jumpCorrection would need to be called
    3 times to correct the latitude, longitude, and altitude separately.

    Jump correction is performed by linearly varying a correction value
    across the jump period which is then applied to the data.

    param: a_data: The data on which we want to perform jump correction.
        This will typically be latitude, longitude, or altitude data, but
        could technically be anything.
    """
    # Use the standard deviation of second order difference to find jumps.
    second = np.diff(a_data, 2)
    differentialMean = np.mean(second)
    differentialStandardDeviation = np.std(second)
    jumpBoolean = ((second - differentialMean) > 2 * differentialStandardDeviation).astype(int)

    # Save indices to jumps. Plus 1 is pre jump. Plus 2 is post jump.
    jumpIndices = np.argwhere(
        jumpBoolean == np.amax(jumpBoolean)).flatten() + 2

    # Correct for the jumps
    for i in range(np.size(jumpIndices)):
        idx = jumpIndices[i]
        # Handle the first jump specially
        previousIdx = 0 if ((i == 0) and (jumpIndices[i] > 3)) \
            else jumpIndices[i - 1]
        jumpDifference = (a_data[idx] - 2 * a_data[idx - 1] + a_data[idx - 2]) \
            / (idx - previousIdx)

        # Linearly vary the correction amount in the range of
        # [0, jumpDifference) across the jump period.
        for j in range(previousIdx, idx):
            a_data[j] += jumpDifference * (j - previousIdx)

    return a_data


class Channel(object):

    def __init__(self, ch_settings=None, band_settings=None, mode=None, is_slimsar=False):
        self.xml = ch_settings
        if is_slimsar:
            if mode is not None:
                # We use the mode for everything
                self.mode = mode
                self.cal_mode = mode - CAL_SAR_SHIFT
                self.ant_num, self.rx_path, self.pol, self.band_num, self.is_dechirp, \
                    self.is_inphase, _ = getSARMode(mode)
            else:
                for cs in ch_settings:
                    if 'Enumeration' in cs:
                        self.mode = int(str(int(ch_settings[cs])), 16)
                        self.cal_mode = self.mode - CAL_SAR_SHIFT
                        self.ant_num, self.rx_path, self.pol, self.band_num, self.is_dechirp, \
                            self.is_inphase, _ = getSARMode(self.mode)
            # Use band settings to determine frequencies
            if band_settings:
                for bs in band_settings:
                    if 'Center_Frequency_Hz' in bs:
                        self.fc = band_settings[bs]
                    if 'Bandwidth_Hz' in bs:
                        self.bw = band_settings[bs]
                    if 'DDS_Settings' in bs:
                        self.xml['Pulse_Length_S'] = \
                            band_settings[bs][bs[:6] + '_DDS_1_Settings'][bs[:6] + '_DDS_1_Pulse_Length_S']
            self.is_lpf = False
            self.fs = 500e6
        else:
            self.is_dechirp = False
            try:
                mode = str(int(ch_settings['Mode']))
            except:
                mode = ch_settings['Mode']
            # Check to see if data is lowpass filtered
            is_lpf = int(mode, 16) & int(0x1000000000)
            try:
                r_fs = fs / ch_settings['Decimation_Factor'] if is_lpf else fs
            except KeyError:
                r_fs = fs
            self.is_lpf = is_lpf
            self.fs = r_fs
            self.fc = ch_settings['Center_Frequency_Hz']
            self.bw = ch_settings['Bandwidth_Hz']
            bboffset = 8e9 if ch_settings['Freq_Band'] == 'X-Band' else 33e9
            bboffset += 1e9 if ch_settings['Upper_Band_Enabled'] == 'true' else 0
            self.baseband_fc = (ch_settings['Center_Frequency_Hz'] - bboffset - abs(ch_settings['NCO_Hz'])) % fs

        self.nsam = None
        self.nframes = None
        self.ncals = 0
        self.data_pts = []
        self.cal_pts = []
        self.sys_time = []
        self.frame_num = []
        self.cals = []
        self.atts = []

    def unwrapSystemTime(self):
        tmp_times = self.sys_time.copy()
        self.sys_time = np.zeros((len(tmp_times),))

        # Unwrap the system time
        jumps = 0
        try:
            self.sys_time[0] = tmp_times[0]
            for nn in range(1, len(tmp_times)):
                if tmp_times[nn] < tmp_times[nn - 1]:
                    jumps += 1
                self.sys_time[nn] = tmp_times[nn] + jumps * INT_MAX
        except IndexError:
            self.sys_time = []


def loadXMLFile(fnme, keep_structure=False):
    """
    Parse the xml header file ('ash') associated with the SAR image data to
    retrieve the relative parameters for the dataset
    """
    # instantiate an object of the W3C Document Object Model (DOM) using the ".ash" file for parsing the xml
    # The subapertures have a different name than full aperture SAR images
    dom = parse_xml(parse(fnme))
    try:
        data = dom['SlimSAR_Configuration']['SlimSAR_Info']
    except:
        data = dom['SlimSDR_Configuration']['SlimSDR_Info']
    if keep_structure:
        return dom
    else:
        return flatten(data)


def flatten(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def parse_xml(xml_object, param={}):
    """Extract and return key/value pairs from xml objects.  This method accepts a DOM xml object"""
    # Loop through each node of the DOM xml object and assign the key/value pairs to the dictionary
    for node in xml_object.childNodes:
        # if the type of node is an element node, then try to extract the key/value
        if node.nodeType == node.ELEMENT_NODE:
            if sum([nn.nodeType == nn.TEXT_NODE for nn in node.childNodes]) == 1:
                # if it has a child node, then we will extract the data
                if node.hasChildNodes():
                    # if the node value contains a letter A-Za-z then interpret it as a string
                    if search('[A-Za-z]|:', node.childNodes[0].nodeValue):
                        param[str(node.nodeName)] = str(node.childNodes[0].nodeValue)
                    # else interpret the node value as a float
                    else:
                        try:
                            param[str(node.nodeName)] = float(node.childNodes[0].nodeValue)
                        except ValueError:
                            param[str(node.nodeName)] = node.childNodes[0].nodeValue
                # else, if there is not a child node, then assign it an empty list
                else:
                    param[str(node.nodeName)] = []
            else:
                param[str(node.nodeName)] = parse_xml(node, {})
    return param


def loadASHFile(filename):
    """
    Parse the xml header file ('ash') associated with the SAR image data to
    retrieve the relative parameters for the dataset
    """
    # instantiate an object of the W3C Document Object Model (DOM) using the ".ash" file for parsing the xml
    # The subapertures have a different name than full aperture SAR images
    dom = parse(filename)
    # The SARImage data type is assigned the xml base node
    datatype = str(dom.childNodes[0].nodeName)
    # pass _parse_xml the DOM object corresponding to each of the sub-parameter fields (or nodes)
    # to extract key/value pairs for a python dictionary data structure.  This is done for File_Parameters,
    # Image_Parameters, Geo_Parameters, Flight_Parameters, Radar_Parameters, Processing_Parameters.
    paramFile = parse_xml(dom.childNodes[0].childNodes[1], {})
    paramImage = parse_xml(dom.childNodes[0].childNodes[3], {})
    paramGeo = parse_xml(dom.childNodes[0].childNodes[5], {})
    paramFlight = parse_xml(dom.childNodes[0].childNodes[7], {})
    paramRadar = parse_xml(dom.childNodes[0].childNodes[9], {})
    paramProc = parse_xml(dom.childNodes[0].childNodes[11], {})
    return {'type': datatype, 'file': paramFile, 'image': paramImage, 'geo': paramGeo, 'flight': paramFlight,
            'radar': paramRadar, 'proc': paramProc}


def loadASIFile(filename, nrows, ncols, scale=1e10):
    """Read in the complex SAR image data and reshape it"""
    # we should really scale the data by a value
    # construct filename from the SAR image object path, name, and .asi
    # open the file for reading
    fid = open(filename, 'r')
    # read in the whole file binary data as type: complex64, and reshape to 2-dimensional numpy array
    data = (np.fromfile(fid, dtype='complex64', count=-1, sep="")).reshape((int(nrows), int(ncols)), order='C') / scale
    return np.array(data)


def findAllFilenames(sar_filename, debug_dir='/home/jeff/repo/Debug'):
    sar_dir = sar_filename.split('/')
    filelist = {}
    sarname = sar_dir[-1].split('.')[0]
    sar_path = ''
    sar_xml_path = ''
    for c in sar_dir[1:-1]:
        sar_path = sar_path + '/' + c
    for c in sar_dir[1:-2]:
        sar_xml_path = sar_xml_path + '/' + c
    # Load files in actual dir
    for (dirpath, dirnames, filenames) in os.walk(sar_path):
        for f in filenames:
            if sarname in f.split('/')[-1].split('.')[0]:
                if f.split('/')[-1].split('.')[-1] not in filelist:
                    filelist[f.split('/')[-1].split('.')[-1]] = {f.split('/')[-1].split('_')[2][-3:]: dirpath + '/' + f}
                else:
                    filelist[f.split('/')[-1].split('.')[-1]][f.split('/')[-1].split('_')[2][-3:]] = dirpath + '/' + f
    # Load files in dir directly above dir (for newer collects where XML
    # is in the upper dir)
    for (dirpath, dirnames, filenames) in os.walk(sar_xml_path):
        for f in filenames:
            if sarname in f.split('/')[-1].split('.')[0]:
                if f.split('/')[-1].split('.')[-1] not in filelist:
                    filelist[f.split('/')[-1].split('.')[-1]] = {f.split('/')[-1].split('_')[2][-3:]: dirpath + '/' + f}
                else:
                    filelist[f.split('/')[-1].split('.')[-1]][f.split('/')[-1].split('_')[2][-3:]] = dirpath + '/' + f
    # Load files in debug directory
    for (dp, dn, f_s) in os.walk(debug_dir):
        for f in f_s:
            if sarname in f.split('/')[-1]:
                dict_key = f.split('/')[-1].split('_')[-1].split('.')[0]
                if dict_key == 'FreqData' or dict_key == 'RawData':
                    if dict_key not in filelist:
                        filelist[dict_key] = {f.split('/')[-1].split('_')[6]: dp + '/' + f}
                    else:
                        filelist[dict_key][f.split('/')[-1].split('_')[6]] = dp + '/' + f
                else:
                    filelist[f.split('/')[-1].split('_')[-1].split('.')[0]] = dp + '/' + f
    for key in filelist:
        if len(filelist[key]) == 1:
            filelist[key] = filelist[key][list(filelist[key].keys())[0]]
    return filelist


def findPowerOf2(x):
    if x < 0:
        return 0
    else:
        x |= x >> 1
        x |= x >> 2
        x |= x >> 4
        x |= x >> 8
        x |= x >> 16
        return x + 1


def lowess(x, y, w, x0=None, f=.1, n_iter=3):
    dn = len(x)
    r = int(np.ceil(f * dn))
    yest = np.zeros(dn)
    delta = np.ones(dn)

    # Looping through all x-points
    for iteration in range(n_iter):
        for i in range(dn):
            weights = np.zeros((dn,))

            # get subset of weight points
            weights[max(0, i - r):min(i + r, dn)] = w[max(0, i - r):min(i + r, dn)]
            weights *= delta
            b = np.array([np.sum(weights * y), np.sum(weights * y * x)])
            A = np.array([[np.sum(weights), np.sum(weights * x)],
                          [np.sum(weights * x), np.sum(weights * x * x)]])
            theta = solve(A, b)
            yest[i] = theta[0] + theta[1] * x[i]

            # use the residuals to modify weighting
        residuals = y - yest
        s = np.median(np.abs(residuals))
        delta = np.clip(residuals / (6.0 * s), -1, 1)
        delta = (1 - delta ** 2) ** 2

    if x0 is None:
        return yest
    else:
        return np.interp(x0, x, yest)


if __name__ == '__main__':
    # Script testing.
    dechirp_fnme = '/data5/SAR_DATA/2020/04142020/SAR_04142020_125610.sar'
    slimsdr_fnme = '/data5/SAR_DATA/2021/08052021/SAR_08052021_111135.sar'
    slimsar_fnme = '/data5/SAR_DATA/2020/03022020/SAR_03022020_112352.sar'
    test_sar = True
    if test_sar:
        sar = SARParse(slimsar_fnme, get_cov=True)
    else:
        sar = SDRParse(slimsdr_fnme)

    gridy = int(np.sqrt(sar.n_channels)) + 1
    gridx = max(sar.n_channels // gridy, 1)
    plt.figure('Real Pulse')
    for n in range(sar.n_channels):
        plt.subplot(gridy, gridx, n + 1)
        plt.title('Channel {}'.format(n))
        pulse_data = sar.getPulse(10, n)
        pulses_data = sar.getPulses([10, 11, 12], n)
        plt.plot(np.real(pulse_data))
        plt.plot(np.real(pulses_data[:, 0]))
    plt.figure('Cal Spectrum')
    for n in range(sar.n_channels):
        plt.subplot(gridy, gridx, n + 1)
        plt.title('Channel {}'.format(n))
        cal_data = sar.getPulse(10, n, is_cal=True)
        cals_data = sar.getPulses([10, 11, 12], n, is_cal=True)
        plt.magnitude_spectrum(cal_data, pad_to=findPowerOf2(sar.channels[n].nsam) * 8,
                               Fs=sar.channels[n].fs, window=lambda x: x)
        plt.magnitude_spectrum(cals_data[:, 0], pad_to=findPowerOf2(sar.channels[n].nsam) * 8,
                               Fs=sar.channels[n].fs, window=lambda x: x)
    plt.figure('GPS Data')
    plt.subplot(2, 2, 1)
    plt.plot(sar.gps_data['lat'])
    plt.plot(sar.bestpos['lat'])
    plt.subplot(2, 2, 2)
    plt.plot(sar.gps_data['lon'])
    plt.plot(sar.bestpos['lon'])
    plt.subplot(2, 2, 3)
    plt.plot(sar.gps_data['alt'])
    plt.plot(sar.bestpos['alt'])
    plt.legend(['BESTPOS', 'INSPVAS'])
    plt.figure('INSCOVS')
    plt.subplot(2, 2, 1)
    plt.title('Pos')
    plt.imshow(sar.inscovs.iloc[0]['Pos'])
    plt.subplot(2, 2, 2)
    plt.title('Att')
    plt.imshow(sar.inscovs.iloc[0]['Att'])
    plt.subplot(2, 2, 3)
    plt.title('Vel')
    plt.imshow(sar.inscovs.iloc[0]['Vel'])
