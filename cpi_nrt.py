import sys
from socket import socket, AF_INET, SOCK_STREAM, create_connection, SOL_SOCKET, SO_REUSEADDR
import array
from PyQt5.QtWidgets import QApplication, QFileDialog, QLabel, QPushButton, QHBoxLayout, QVBoxLayout, QMainWindow, \
    QFrame, QGridLayout, QToolBar, QWidget, QSpinBox
from PyQt5.QtCore import QObject, pyqtSlot, pyqtSignal, QThread, QMutex
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import numpy as np

START_WORD = b'\x0A\xB0\xFF\x18'
STOP_WORD = b'\xAA\xFF\x11\x55'
COLLECTION_MODE_DIGITAL_CHANNEL_MASK = 0x000003E000000000
COLLECTION_MODE_OPERATION_MASK = 0x1800000000000000
COLLECTION_MODE_DIGITAL_CHANNEL_SHIFT = 37
COLLECTION_MODE_OPERATION_SHIFT = 59
REC_CHUNK_SZ = 4096
HEADER_SZ = 28


# Converts data to dB scale
def db(data):
    db_data = abs(data)
    db_data[db_data == 0] = 1e-15
    return 20 * np.log10(db_data)


# Finds the next power of 2 above x
def findPowerOf2(x):
    if x < 0:
        return 0
    else:
        x -= 1
        x |= x >> 1
        x |= x >> 2
        x |= x >> 4
        x |= x >> 8
        x |= x >> 16
        return x + 1


# Window object - for viewing a single pulse's characteristics
class QuickView(QWidget):
    def __init__(self, pulse, mf=None):
        super().__init__()
        layout = QVBoxLayout()
        self.label = QLabel("Single Pulse Check")
        layout.addWidget(self.label)

        # Load display and add to layout
        self.disp = MatplotlibWidget()
        layout.addWidget(self.disp)

        # Buttons layout
        buttons = QHBoxLayout()
        spec_but = QPushButton('Spectrum')
        rc_but = QPushButton('Range Compression')
        time_but = QPushButton('Time Series')
        if mf is None:
            rc_but.setVisible(False)

        # Connect buttons to their respective functions
        spec_but.clicked.connect(lambda: self.drawSpectrum())
        rc_but.clicked.connect(lambda: self.drawRC())
        time_but.clicked.connect(lambda: self.draw())
        buttons.addWidget(spec_but)
        buttons.addWidget(rc_but)
        buttons.addWidget(time_but)
        layout.addStrut(1)
        layout.addLayout(buttons)
        layout.addStretch(1)
        self.setLayout(layout)
        self._pulse = pulse
        self._mf = mf
        self.draw()

    # If the user wants to view another pulse, redraw the window
    def redraw(self, pulse):
        self._pulse = pulse
        self.draw()

    # Draws the time domain pulse
    def draw(self):
        xes = np.arange(len(self._pulse))
        self.disp.plot(xes, np.real(self._pulse), y2=np.imag(self._pulse))

    # Draws the frequency spectrum in dB
    def drawSpectrum(self):
        self.disp.plotSpectrum(self._pulse, fs=2e9, pad_to=findPowerOf2(len(self._pulse)) * 4)

    # Draws the range compressed pulse in the time domain (dB scale)
    def drawRC(self):
        xes = np.arange(len(self._mf) * 4)
        self.disp.plot(xes, db(np.fft.ifft(np.fft.fft(self._pulse, n=len(self._mf)) * self._mf.conj().T,
                                           n=len(self._mf) * 4)))


# Configuration window, shows some useful stuff from the configuration packet
class ConfigWindow(QFrame):
    def __init__(self, config):
        super().__init__()
        infoLayout = QGridLayout()
        try:
            datalist = [('Alt (ft)', config['alt']), ('Vel(knots)', config['vel']),
                        ('PRF Broadening Factor', config['prfb']), ('Near Range Angle', config['near_range']),
                        ('Far Range Angle', config['far_range']), ('Doppler PRF (Hz)', config['dopp_prf']),
                        ('Center Freq (MHz)', config['fc'] / 1e6), ('Bandwidth (MHz)', config['bw'] / 1e6),
                        ('Cal Pulses Received', config['calrec'])]
            for idx, l in enumerate(datalist):
                infoLayout.addWidget(QLabel(l[0]), idx, 0)
                infoLayout.addWidget(QLabel('{:.2f}'.format(l[1])), idx, 1)
        except KeyError:
            infoLayout.addWidget(QLabel('No config data found.'), 0, 0)
        layout = QVBoxLayout()
        layout.addWidget(QLabel('Configuration Data'))
        layout.addLayout(infoLayout)
        self.setLayout(layout)


# This is the main window. Starts the data/control servers, displays CPIs, and
# contains settings information.
class CPI_Window(QMainWindow):
    _signal_statusbar = pyqtSignal(str)
    _signal_singlepulse = pyqtSignal()
    _signal_filterdone = pyqtSignal()
    _signal_stopprocess = pyqtSignal()
    _signal_plotcpi = pyqtSignal()
    _signal_plotmf = pyqtSignal()
    _signal_status2 = pyqtSignal(str)
    _signal_timer = pyqtSignal(bool)
    """Main Window."""

    def __init__(self, parent=None):
        """Initializer."""
        super().__init__(parent)

        self.port_address = 11788
        self.cpi_len = 128
        self.data_address = None
        self.matched_filter = None
        self.mfcount = 0
        self._hasFilter = False
        self.fft_len = 0
        self.cpi = []
        self.config = {'pdrop': 0, 'calrec': 0}
        self.con_win = None

        # Connect all the signals to their slots
        self._signal_statusbar.connect(self._slotStatusMessage)
        self._signal_status2.connect(self._slotStatus2)
        self._signal_plotcpi.connect(self._slotPlotCPI)
        self._signal_singlepulse.connect(self._slotPlotPulse)
        self._signal_plotmf.connect(self._slotPlotMF)
        self._signal_timer.connect(self._slotTimeout)
        self._signal_filterdone.connect(self._slotGenFilter)

        # Parse Layout widgets
        socket_layout = QHBoxLayout()
        self.port_box = QSpinBox()
        self.port_box.setMaximum(50000)
        self.port_box.setMinimum(0)
        self.port_box.setValue(self.port_address)
        listen_but = QPushButton('Listen')
        self.cpi_but = QPushButton('Show CPI')
        self.cpi_but.setDisabled(True)
        self.single_but = QPushButton('Show Single Pulse')
        self.single_but.setDisabled(True)
        self.mf_but = QPushButton('Show Matched Filter')
        self.mf_but.setDisabled(True)
        stop_but = QPushButton('Stop')

        # Add widgets to layout
        socket_layout.addWidget(QLabel('Port:'))
        socket_layout.addWidget(self.port_box)
        socket_layout.addWidget(listen_but)
        socket_layout.addWidget(self.cpi_but)
        socket_layout.addWidget(self.single_but)
        socket_layout.addWidget(stop_but)
        socket_layout.addWidget(self.mf_but)
        socket_layout.addStretch(1)

        # Instantiate listeners in their threads
        self.control_server = ControlServer(self.port_address)
        self.control_server.sig_controlupdate.connect(self._slotUpdateControlInfo)
        self.control_server.sig_controlreceived.connect(self._slotReceiveData)
        self._signal_stopprocess.connect(self.control_server.stop)
        self.control_listener = QThread()
        self.control_listener.start()
        self.control_server.moveToThread(self.control_listener)
        self.control_server.sig_start.connect(self.control_server.run)
        self.control_listener.finished.connect(lambda: print('Control Server stopped.'))
        self.statusBar().showMessage('Currently set to port {}'.format(self.port_address))

        # The data server doesn't turn on until the control server recieves a connection
        self.data_listener = None

        # Connect all the buttons to their signals
        listen_but.clicked.connect(lambda: self._slotChangePort())
        self.cpi_but.clicked.connect(lambda: self._signal_plotcpi.emit())
        self.single_but.clicked.connect(lambda: self._signal_singlepulse.emit())
        stop_but.clicked.connect(lambda: self._signal_stopprocess.emit())
        self.mf_but.clicked.connect(lambda: self._signal_plotmf.emit())

        # Opts Layout widgets
        opts_layout = QHBoxLayout()
        self.cpi_len_box = QSpinBox()
        self.cpi_len_box.setMaximum(15000)
        self.cpi_len_box.setMinimum(16)
        self.cpi_len_box.setValue(self.cpi_len)
        self.cpi_len_box.valueChanged.connect(self._setCPILength)

        # Opts layout add widgets
        opts_layout.addWidget(QLabel('CPI Length: '))
        opts_layout.addWidget(self.cpi_len_box)
        opts_layout.addStretch(1)

        # Control server info layout
        control_layout = QHBoxLayout()
        self.control_info = QLabel('No control server connected.')
        control_layout.addWidget(QLabel('Control: '))
        control_layout.addWidget(self.control_info)
        control_layout.addStretch(1)

        # Data server info layout
        data_layout = QHBoxLayout()
        self.data_info = QLabel('No data server connected.')
        data_layout.addWidget(QLabel('Data: '))
        data_layout.addWidget(self.data_info)
        data_layout.addStretch(1)

        # Status bar info layout
        self.frame_update = QLabel('No collect')
        self.statusBar().addPermanentWidget(VLine())
        self.statusBar().addPermanentWidget(self.frame_update)

        # Main window
        window = QWidget()
        window.setWindowTitle('Spectrum Analyzer')
        layout = QVBoxLayout()
        self.display = MatplotlibWidget()

        # Add all the other layouts to the main display
        layout.addWidget(self.display)
        layout.addLayout(socket_layout)
        layout.addLayout(opts_layout)
        layout.addLayout(control_layout)
        layout.addLayout(data_layout)
        window.setLayout(layout)

        # Menus and toolbars for the main window
        self.setCentralWidget(window)
        self._createMenu()
        self._createToolBar()

    def _createMenu(self):
        self.menu = self.menuBar().addMenu("&Menu")
        self.menu.addAction('&Exit', self.close)

    def _createToolBar(self):
        tools = QToolBar()
        self.addToolBar(tools)
        tools.addAction('Exit', self.close)
        tools.addAction('Show Config', self._showConfig)

    def _showConfig(self):
        self.con_win = ConfigWindow(self.config)
        self.con_win.show()

    @pyqtSlot()
    def _slotPlotCPI(self):
        fft_cpi = np.fft.fft(np.array(self.cpi), n=self.fft_len, axis=1)
        # If we have doppler information we can display frequencies, otherwise use default
        if 'dopp_prf' in self.config:
            freqs = np.fft.fftshift(np.fft.fftfreq(self.cpi_len, 1 / self.config['dopp_prf']))
        else:
            freqs = np.fft.fftshift(np.fft.fftfreq(self.cpi_len))
        # Range compress if we have a matched filter
        if self._hasFilter:
            fft_cpi = np.fft.ifft(fft_cpi * self.matched_filter.conj().T[None, :], axis=1)
        fft_cpi = np.fft.fft(fft_cpi, n=self.cpi_len, axis=0)
        self.display.imshow(np.fft.fftshift(db(fft_cpi)).T, extent=[freqs[0], freqs[-1], 0, self.fft_len],
                            origin='lower')

    @pyqtSlot()
    def _slotTimeout(self):
        self.data_listener.terminate()
        self.data_listener.wait()
        self._signal_statusbar.emit('Data server timed out.')

    @pyqtSlot()
    def _slotGenFilter(self):
        if not self._hasFilter:
            mf = self.matched_filter / self.mfcount
            self.matched_filter = np.fft.fft(mf, self.fft_len)  # * taylor(self.fft_len, nbar=6, sll=30)
            self._hasFilter = True
            self._signal_statusbar.emit('Matched Filter generated.')
            print('Matched Filter generated.')
            self.mf_but.setDisabled(False)
            self.cpi_but.setDisabled(False)
            self.single_but.setDisabled(False)

    def _setCPILength(self):
        self.cpi_len = self.cpi_len_box.value()

    def closeEvent(self, event):
        # In order to close correctly, all threads must be stopped
        self._signal_stopprocess.emit()
        self.control_listener.quit()
        self.control_listener.wait()
        if self.data_listener is not None:
            self.data_listener.quit()
            self.data_listener.wait()
        event.accept()

    def resetDataServer(self, pno):
        # Start the data server
        self.matched_filter = None
        self.mfcount = 0
        self._hasFilter = False
        self.fft_len = 0
        self.cpi = []
        self.config = {'pdrop': 0, 'calrec': 0}
        self.con_win = None
        # If there's already a data server, quit and restart it
        if self.data_listener is not None:
            if self.data_listener.isRunning():
                self.data_listener.quit()
                self.data_listener.wait()
        self.data_listener = DataServer(pno)
        self.data_listener.start()

        # Connect all the signals
        self.data_listener.sig_filterdone.connect(self._slotGenFilter)
        self.data_listener.sig_pulsedata.connect(self._slotParseData)
        self.data_listener.sig_filterdata.connect(self._slotParseCalData)
        self.data_listener.sig_dataupdate.connect(self._slotUpdateDataInfo)
        self.data_listener.sig_statusupdate.connect(self._slotStatus2)
        self._signal_stopprocess.connect(self.data_listener.stop)
        self.data_listener.finished.connect(lambda: print('Data Server stopped.'))
        self.data_address = pno

    @pyqtSlot()
    def _slotPlotPulse(self):
        swin = QuickView(self.cpi[0], self.matched_filter)
        swin.show()

    @pyqtSlot()
    def _slotPlotMF(self):
        # QuickView expects time domain data and we store the matched filter in
        # the frequency domain
        mf = np.fft.ifft(self.matched_filter)
        swin = QuickView(mf, self.matched_filter)
        swin.show()

    @pyqtSlot(str)
    def _slotStatusMessage(self, message):
        self.statusBar().showMessage(message)

    @pyqtSlot(str)
    def _slotStatus2(self, message):
        self.frame_update.setText(message)

    @pyqtSlot(str)
    def _slotUpdateDataInfo(self, message):
        message = 'Port {}, '.format(self.data_address) + message
        self.data_info.setText(message)

    @pyqtSlot(str)
    def _slotUpdateControlInfo(self, message):
        message = 'Port {}, '.format(self.port_address) + message
        self.control_info.setText(message)

    @pyqtSlot(bytes)
    def _slotReceiveData(self, rec_packet):
        # Check the packet type to send info to proper place
        if rec_packet[0] == 0x01:
            pno = int.from_bytes(rec_packet[3:], 'big', signed=False)
            # if self.data_address is None:
            self.resetDataServer(pno)
        elif rec_packet[0] == 0x02:
            # This is the configuration packet, with all the startup data
            numModes = int.from_bytes(rec_packet[56:60], 'little', signed=False)
            config_stuff = array.array('f', rec_packet[62:78])
            ranges = array.array('d', rec_packet[1282:1322])
            doppPRF = array.array('d', rec_packet[1367:1375])
            mode = array.array('d', rec_packet[1257:1289])
            self.config['n_modes'] = numModes
            self.config['alt'] = config_stuff[1]
            self.config['vel'] = config_stuff[2]
            self.config['prfb'] = config_stuff[3]
            self.config['pri'] = int.from_bytes(rec_packet[78:82], 'little', signed=False) / 125e6
            self.config['n_cal'] = int.from_bytes(rec_packet[82:86], 'little', signed=False)
            self.config['dopp_prf'] = doppPRF[0]
            self.config['near_range'] = ranges[0]
            self.config['far_range'] = ranges[1]
            self.config['presum'] = rec_packet[1314]
            self.config['fc'] = mode[1]
            self.config['bw'] = mode[2]
            self._signal_statusbar.emit('Configuration loaded.')
        elif rec_packet[0] == 0x03:
            # This is a data packet
            pass
        elif rec_packet[0] == 0x04:
            # Collection done packet
            pass

    @pyqtSlot(str)
    def _slotChangePort(self):
        self.port_address = self.port_box.value()
        self._signal_stopprocess.emit()
        self.control_server.sig_portchange.emit(self.port_address)
        self.control_server.initConnection()
        self.control_server.sig_start.emit()
        self.data_listener = None

    @pyqtSlot()
    def _slotPacketDropped(self):
        self.config['pdrop'] += 1

    @pyqtSlot(object)
    def _slotParseData(self, data):
        if len(self.cpi) >= self.cpi_len:
            self.cpi.pop(0)
        self.cpi.append(data)

    @pyqtSlot(object)
    def _slotParseCalData(self, data):
        if self.matched_filter is None:
            self.matched_filter = data
            self.fft_len = findPowerOf2(len(data))
        else:
            pass
            self.matched_filter += data
        self.mfcount += 1
        self.config['calrec'] = self.mfcount


class DataServer(QThread):
    sig_dataupdate = pyqtSignal(str)
    sig_filterdone = pyqtSignal()
    sig_filterdata = pyqtSignal(object)
    sig_pulsedata = pyqtSignal(object)
    sig_statusupdate = pyqtSignal(str)

    def __init__(self, pno):
        super().__init__()
        self.address = pno
        self.shutdown = False
        self.mfcount = 0

        # Given the port address, initiate connection
        try:
            self.sock = create_connection(('localhost', self.address))
        except PermissionError:
            self.sig_dataupdate.emit("Permission Denied.")
            return
        except OSError:
            self.sig_dataupdate.emit("Address already in use.")
            return
        self.sig_dataupdate.emit("Connected.")

        # This ensures the thread stops when data stops being sent to it
        self.sock.settimeout(20)

    def run(self):
        hasfilter = False
        try:
            self.sig_dataupdate.emit("Receiving.")
            try:
                data = surerec(self.sock, REC_CHUNK_SZ)
            except OSError:
                self.sig_dataupdate.emit("Nothing received.")
                return

            # Run through data chunks, finding packets and interpreting them
            while not self.shutdown:
                st_word = data.find(START_WORD)
                # Start word is found, we have a data packet
                if st_word != -1:
                    data = data[st_word + 4:]
                    # If start word is at the end of a chunk, read in enough to get contiguous header data
                    if len(data) < HEADER_SZ:
                        data = data + surerec(self.sock, HEADER_SZ - len(data))

                    # Header data
                    frame = int.from_bytes(data[:4], byteorder='big', signed=False)
                    systime = int.from_bytes(data[4:8], byteorder='big', signed=False)
                    mode = int.from_bytes(data[8:16], byteorder='big', signed=False)
                    is_cal = (mode & COLLECTION_MODE_OPERATION_MASK) >> COLLECTION_MODE_OPERATION_SHIFT
                    nsam = int.from_bytes(data[16:20], byteorder='big', signed=False)
                    # print(nsam)
                    att = data[20] & 0x1f
                    self.sig_statusupdate.emit('Frame {}, systime {}, is_cal {}, nsam {}'.format(frame, systime, is_cal, nsam))
                    data = data[HEADER_SZ:]

                    # Now, hunt for the stop word
                    while data.find(STOP_WORD != -1) and len(data) < nsam * 4:
                        try:
                            nd = surerec(self.sock, REC_CHUNK_SZ)
                        except OSError:
                            self.sig_dataupdate.emit("Connection timed out.")
                            return
                        data = data + nd
                        if not nd:
                            return
                    sp_word = data.find(STOP_WORD)

                    # We've found us a stop word, load in the pulse
                    if sp_word != -1:
                        pulse = np.zeros((nsam,), dtype=np.complex128)
                        ndata = array.array('h', data[:sp_word])
                        ndata.byteswap()  # Little endian data
                        ndata = np.array(ndata)
                        pulse[:nsam] = (ndata[0:nsam * 2:2] + 1j * ndata[1:nsam * 2:2]) * (10 ** (att / 20))
                        if is_cal:
                            # Calibration data gets sent to the matched filter
                            self.sig_filterdata.emit(pulse)
                            self.mfcount += 1
                        else:
                            # Signal data gets sent to be part of the CPI
                            self.sig_pulsedata.emit(pulse)

                        # We assume all the calibration data is sent in a chunk
                        # so we'll have a matched filter for the CPIs later
                        if self.mfcount >= 100 and not is_cal and not hasfilter:
                            self.sig_filterdone.emit()
                            hasfilter = True
                            # print('FILTER DONE')
                        data = data[sp_word + 4:]
                else:
                    # No start word, grab another chunk
                    try:
                        nd = surerec(self.sock, REC_CHUNK_SZ)
                    except OSError:
                        self.sig_dataupdate.emit("Connection timed out.")
                        return  # Abort if no data is sent
                    data = data + nd
                    if not nd:
                        return  # Abort if no data is sent
                if not data:
                    return  # Abort if no data is sent
            self.sig_dataupdate.emit("Connection closed.")
        finally:
            self.sock.close()
        self.sig_dataupdate.emit("Connection closed.")

    @pyqtSlot()
    def stop(self):
        self.shutdown = True


def surerec(sock, sz):
    data = sock.recv(sz)
    while len(data) < sz:
        data = data + sock.recv(sz - len(data))
    return data


class ControlServer(QObject):
    sig_controlupdate = pyqtSignal(str)
    sig_controlreceived = pyqtSignal(bytes)
    sig_start = pyqtSignal()
    sig_portchange = pyqtSignal(int)

    def __init__(self, pno):
        super().__init__()
        self.sock = None
        self.address = pno
        self._running = True
        self._mutex = QMutex()
        self.initConnection()
        self.sig_start.connect(self.run)
        self.sig_portchange.connect(self.changePort)

    def initConnection(self):
        # Create a server and connect to a socket
        # Strider should be connecting to this socket as well
        self.sock = None
        self.sock = socket(AF_INET, SOCK_STREAM)
        self.sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        try:
            self.sock.bind(('0.0.0.0', self.address))
        except PermissionError:
            self.sig_controlupdate.emit("Permission Denied.")
            return
        except OSError:
            self.sig_controlupdate.emit("Address already in use.")
            return
        self.sock.listen(1)
        self.sig_controlupdate.emit("Listening.")
        self._running = True

    @pyqtSlot()
    def run(self):
        connection = None
        # This runs on its own thread, listening for connections
        while self.running():
            try:
                connection, client_address = self.sock.accept()
            except OSError:
                self.initConnection()
                connection, client_address = self.sock.accept()
            connection.send(bytes([0, 0, 0, 1]))
            break

        # Connection found!
        while self.running():
            try:
                self.sig_controlupdate.emit("Connected.")
                while self.running():
                    try:
                        data = connection.recv(1)
                    except OSError as e:
                        self.sig_controlupdate.emit(str(e))
                        data = []
                    if not data:
                        return  # Abort if no data is sent
                    if data[0] == 0x00:
                        self.sig_controlupdate.emit('Status packet received.')
                    elif data[0] == 0x01:
                        self.sig_controlupdate.emit('SysInfo packet received.')
                        data += connection.recv(4)
                        # Tell Strider it's OK to send data
                        connection.send(bytes([0, 1, 0, 1]))
                    elif data[0] == 0x02:
                        self.sig_controlupdate.emit('Collection started packet received.')
                        # Just grab everything
                        data += connection.recv(8192)
                    elif data[0] == 0x03:
                        self.sig_controlupdate.emit('Data packet received.')
                    elif data[0] == 0x04:
                        self.sig_controlupdate.emit('Collection done packet received.')
                        # Tell Strider it's over between us
                        connection.send(bytes([0, 0, 0, 1]))
                        self._running = False  # Stop listening
                    else:
                        self.sig_controlupdate.emit('Unknown packet received.')
                    self.sig_controlreceived.emit(data)
            finally:
                connection.close()
                self.sig_controlupdate.emit("Connection closed.")

    @pyqtSlot()
    def stop(self):
        self._mutex.lock()
        self._running = False
        self._mutex.unlock()

    @pyqtSlot(int)
    def changePort(self, pno):
        self.address = pno

    def running(self):
        try:
            self._mutex.lock()
            return self._running
        finally:
            self._mutex.unlock()


class VLine(QFrame):
    # a simple VLine, makes the status bar look classier
    def __init__(self):
        super(VLine, self).__init__()
        self.setFrameShape(self.VLine | self.Sunken)


class MatplotlibWidget(QWidget):

    # Generates a Matplotlib plot inside of a QWidget, with all the nice
    # zoom/save features
    def __init__(self, size=(5.0, 4.0), dpi=100):
        QWidget.__init__(self)
        plt.ion()
        self.fig = Figure(size, dpi=dpi)
        self.ax = self.fig.add_subplot(111)

        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self)
        self.toolbar = NavigationToolbar(self.canvas, self)

        self.vbox = QVBoxLayout()
        self.vbox.addWidget(self.toolbar)
        self.vbox.addWidget(self.canvas)
        self.ylim = [100, 1]

        self.setLayout(self.vbox)

    def set_ylim(self, min_lim, max_lim):
        self.ylim = [min_lim, max_lim]

    # Mimics matplotlib.pyplot.plot
    def plot(self, x, y, y2=None, xlabel='Time', ylabel='Amplitude', title=''):
        self.ax.cla()
        self.ax.set_ylabel(ylabel)
        self.ax.set_xlabel(xlabel)
        self.ax.plot(x, y, 'b-')
        if y2 is not None:
            self.ax.plot(x, y2, 'r-')
            self.ax.legend(['Real', 'Imag'])
        self.ax.set_title(title)

        self.canvas.draw()
        self.canvas.flush_events()

    # Mimics matplotlib.pyplot.magnitude_spectrum
    def plotSpectrum(self, x, fs=2, xlabel='Freq', pad_to=None, scale='dB', title=''):
        self.ax.cla()
        self.ax.set_ylabel(scale)
        self.ax.set_xlabel(xlabel)
        self.ax.magnitude_spectrum(x, Fs=fs, pad_to=pad_to, scale=scale, window=lambda xx: xx)
        self.ax.set_title(title)
        self.canvas.draw()
        self.canvas.flush_events()

    # Mimics matplotlib.pyplot.imshow
    def imshow(self, data, extent=None, origin='upper', xlabel='Freq(Hz)', ylabel='Bins'):
        self.ax.cla()
        self.ax.set_ylabel(ylabel)
        self.ax.set_xlabel(xlabel)
        self.ax.imshow(data, extent=extent, origin=origin, cmap='jet')
        self.ax.axis('tight')
        self.canvas.draw()
        self.canvas.flush_events()


if __name__ == '__main__':
    # Instantiate the window
    app = QApplication(sys.argv)
    win = CPI_Window()
    win.show()

    # Run main loop
    sys.exit(app.exec_())
