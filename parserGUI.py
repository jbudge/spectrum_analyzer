#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 10:00:45 2021

@author: jeff
"""

import sys

from PyQt5.QtWidgets import QApplication, QFileDialog
from PyQt5.QtWidgets import QLabel, QPushButton, QHBoxLayout, QVBoxLayout
from PyQt5.QtWidgets import QMainWindow, QCheckBox
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QToolBar, QWidget
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import numpy as np
import pylab as plab
# from celluloid import Camera
from parsing import SARMiniParser, SDRMiniParser


def db(data):
    db_data = abs(data)
    db_data[db_data == 0] = 1e-15
    return 20 * np.log10(db_data)


def findPowerOf2(x):
    if x < 0:
        return 0
    else:
        --x
        x |= x >> 1
        x |= x >> 2
        x |= x >> 4
        x |= x >> 8
        x |= x >> 16
        return x + 1


class Window(QMainWindow):
    """Main Window."""

    def __init__(self, parent=None):
        """Initializer."""
        super().__init__(parent)

        # Variables for data parsing and manipulation
        self.sar = None
        self.fft_sz = 0
        self.stop_parsing = False

        # Parse Layout widgets
        parse_layout = QHBoxLayout()
        self.file = QLineEdit('/data5/SAR_DATA/2019/06102019/SAR_06102019_133035.sar')
        file_but = QPushButton('Select')
        parse_but = QPushButton('Parse')
        single_but = QPushButton('Single Plot')
        self.dechirp_checkbox = QCheckBox('SDR')

        # Add widgets to layout
        parse_layout.addWidget(QLabel('File:'))
        parse_layout.addWidget(self.file)
        parse_layout.addWidget(file_but)
        parse_layout.addWidget(parse_but)
        parse_layout.addWidget(self.dechirp_checkbox)

        # Parse Layout Slots
        parse_but.clicked.connect(self.slot_parseFile)

        # Opts Layout widgets
        opts_layout = QHBoxLayout()
        self.ptoavbox = QLineEdit('30')
        self.ptorunstartbox = QLineEdit('0')
        self.ptorunendbox = QLineEdit('2000')
        self.psingle = QLineEdit('0')

        # Opts layout add widgets
        opts_layout.addWidget(QLabel('Pulses to Average:'))
        opts_layout.addWidget(self.ptoavbox)
        opts_layout.addWidget(QLabel('Pulses to Run:'))
        opts_layout.addWidget(self.ptorunstartbox)
        opts_layout.addWidget(QLabel(' to '))
        opts_layout.addWidget(self.ptorunendbox)
        opts_layout.addWidget(single_but)
        opts_layout.addWidget(self.psingle)

        # Opts layout slots
        single_but.clicked.connect(self.slot_singlePulse)

        # Button Layout
        button_layout = QHBoxLayout()
        self.run_but = QPushButton('Run')
        stop_but = QPushButton('Stop')

        # Button layout add widgets
        button_layout.addWidget(parse_but)
        button_layout.addWidget(self.run_but)
        button_layout.addWidget(stop_but)

        # Button layout slots
        self.run_but.clicked.connect(self.slot_runAnalyzer)
        stop_but.clicked.connect(self.slot_stop)
        file_but.clicked.connect(self.slot_getFile)
        self.run_but.setVisible(False)

        # Main window
        window = QWidget()
        window.setWindowTitle('Spectrum Analyzer')
        layout = QVBoxLayout()
        self.display = MatplotlibWidget()

        # Add all the other layouts to the main display
        layout.addWidget(self.display)
        layout.addLayout(parse_layout)
        layout.addLayout(opts_layout)
        layout.addLayout(button_layout)
        window.setLayout(layout)

        # Menus and toolbars for the main window
        self.setCentralWidget(window)
        self._createMenu()
        self._createToolBar()
        self.statusBar().showMessage('Ready.')

    def _createMenu(self):
        self.menu = self.menuBar().addMenu("&Menu")
        self.menu.addAction('&Exit', self.close)

    def _createToolBar(self):
        tools = QToolBar()
        self.addToolBar(tools)
        tools.addAction('Exit', self.close)
        tools.addAction('Parse', self.slot_parseFile)
        tools.addAction('Run', self.slot_runAnalyzer)

    def slot_parseFile(self):
        if self.dechirp_checkbox.isChecked():
            self.statusBar().showMessage('Parsing SDR file...')
            self.repaint()
            self.sar = SDRMiniParser(self.file.text())
            self.statusBar().showMessage('SDR file parsed.')
        else:
            self.statusBar().showMessage('Parsing SAR file...')
            self.repaint()
            self.sar = SARMiniParser(self.file.text())
            self.statusBar().showMessage('SAR file parsed.')
        self.fft_sz = findPowerOf2(self.sar.n_samples[0]) * 4
        self.run_but.setVisible(True)

    def slot_getFile(self):
        fnme = QFileDialog(directory='/data5/SAR_DATA')
        fnme.setFileMode(QFileDialog.AnyFile)
        if fnme.exec_():
            file_to_get = fnme.selectedFiles()
            self.file.setText(file_to_get[0])

    def slot_runAnalyzer(self):
        self.stop_parsing = False
        if self.dechirp_checkbox.isChecked():
            samples = plab.fftshift(plab.fftfreq(self.fft_sz, 1 / 2e9))
        else:
            samples = plab.fftfreq(self.fft_sz, 1 / 500e6)[:self.fft_sz // 2]
        st_pulse = int(self.ptorunstartbox.text())
        end_pulse = int(self.ptorunendbox.text())
        if end_pulse > self.sar.frameNum[-1]:
            end_pulse = self.sar.frameNum[-1]
        for n in np.arange(st_pulse, end_pulse, int(self.ptoavbox.text())):
            if self.stop_parsing:
                self.statusBar().showMessage('Run cancelled.')
                break
            frames = np.mean(self.sar.getPulses(np.arange(n, n + int(self.ptoavbox.text()))), axis=0)
            if self.dechirp_checkbox.isChecked():
                frames = plab.fftshift(db(plab.fft(frames, self.fft_sz)))
            else:
                frames = db(plab.fft(frames, self.fft_sz))[:self.fft_sz // 2]
            if frames.max() > self.display.ylim[1] or frames.min() < self.display.ylim[0]:
                self.display.set_ylim(min(self.display.ylim[0], frames.min() - 10),
                                      max(self.display.ylim[1], frames.max() + 3))

            self.display.plot(samples,
                              frames,
                              title='Pulses {}-{}'.format(n, n + int(self.ptoavbox.text())))
        self.stop_parsing = False

    def slot_singlePulse(self):
        pnum = int(self.psingle.text())
        if self.dechirp_checkbox.isChecked():
            samples = plab.fftshift(plab.fftfreq(self.fft_sz, 1 / 2e9))
            init_y = db(plab.fftshift(plab.fft(self.sar.getPulse(pnum), self.fft_sz)))
        else:
            samples = plab.fftfreq(self.fft_sz, 1 / 500e6)[:self.fft_sz // 2]
            init_y = db(plab.fft(self.sar.getPulse(pnum), self.fft_sz)[:self.fft_sz // 2])
        self.display.set_ylim(init_y.min() - 10, init_y.max() + 3)
        self.display.plot(samples, init_y, title='Pulse {}'.format(pnum))
        self.statusBar().showMessage('Single pulse plotted.')

    def slot_stop(self):
        self.stop_parsing = True


class MatplotlibWidget(QWidget):

    def __init__(self, size=(5.0, 4.0), dpi=100):
        QWidget.__init__(self)
        plt.ion()
        self.fig = Figure(size, dpi=dpi)
        self.ax = self.fig.add_subplot(111)

        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self)
        self.toolbar = NavigationToolbar(self.canvas, self)

        self.vbox = QVBoxLayout()
        self.vbox.addWidget(self.toolbar)
        self.vbox.addWidget(self.canvas)
        self.ylim = [100, 1]

        self.setLayout(self.vbox)

    def set_ylim(self, min_lim, max_lim):
        self.ylim = [min_lim, max_lim]

    def plot(self, x, y, title=''):
        self.ax.cla()
        self.ax.set_ylabel('Power (dB)')
        self.ax.set_xlabel('Freq. (Hz)')
        self.ax.plot(x, y, 'b-')
        self.ax.set_title(title)
        self.ax.set_ylim(self.ylim)

        self.canvas.draw()
        self.canvas.flush_events()


# Instantiate the window
if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = Window()
    win.show()

    # Run main loop
    sys.exit(app.exec_())
