import numpy as np
import matplotlib.pyplot as plt
from aps_io import getRawDataGen, getRawData, loadGPSData, loadGimbalData, loadXMLFile, getBasebandCenterFrequency, \
    loadReferenceChirp


def findPowerOf2(x):
    if x < 0:
        return 0
    else:
        x |= x >> 1
        x |= x >> 2
        x |= x >> 4
        x |= x >> 8
        x |= x >> 16
        return x + 1


def db(data):
    db_data = abs(data)
    try:
        db_data[db_data == 0] = 1e-15
    except TypeError:
        if db_data == 0:
            db_data = 1e-15
    return 20 * np.log10(db_data)


# System constants
fs = 2e9  # Sampling frequency
TAC = 125e6  # Clock frequency
c0 = 299792458.0  # Speed of light in m/s

# .SAR filename to get debug outputs
fnme = 'SAR_08052021_111135'

# Directory for debug outputs
parse_debug_dir = '/data5/JEFFFFFFFF'
debug_dir = '/data5/SAR_Freq_Data/08052021'

# XML file associated with .SAR file
xml_fnme = '/data5/SAR_DATA/2021/08052021/SAR_08052021_111135.xml'

# Number of pulses in CPI
cpi_length = 256
interp_factor = 2

# Parse XML data into dict
xml_data = loadXMLFile(xml_fnme, keep_structure=True)

# Get some relevant parameters from the XML
bandwidth = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_1']['Bandwidth_Hz']
vel = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Common_Channel_Settings']['Velocity_Knots']
pulse_length = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_1']['Pulse_Length_S']
is_upper_band = True if xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_1']['Upper_Band_Enabled'] == 'true' else False
nco_hz = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_1']['NCO_Hz']
fc = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_1']['Center_Frequency_Hz']
band = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_1']['Freq_Band']
h_agl = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Common_Channel_Settings']['Altitude_Ft'] / 3.2808
dopp_prf = xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Common_Channel_Settings']['Transmit_PRF_Hz'] / \
           xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_1']['Presum_Factor']
near_range = ((xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_1']['Receive_On_TAC'] -
               xml_data['SlimSDR_Configuration']['SlimSDR_Info']['Channel_1']['Transmit_On_TAC']) / TAC) * c0 / 2
far_range = near_range + pulse_length * fs * c0 / 2 / fs
near_range = np.sqrt(near_range ** 2 - h_agl ** 2)
far_range = np.sqrt(far_range**2 - h_agl**2)


# Calculate basebanded center frequency
fc_baseband = getBasebandCenterFrequency(band, fc, nco_hz, is_upper_band=is_upper_band)

# Load in reference chirp
# This is made from calibration data and is for matched filter creation
if True:
    # ref_chirp = loadReferenceChirp(parse_debug_dir + '/' + fnme + '_Ref_Chirp_Channel_2.dat')
    ref_chirp = loadReferenceChirp(debug_dir + '/' + fnme + '_Channel_2_ReferenceChirp.dat')

    fft_len = findPowerOf2(ref_chirp.shape[0]) * interp_factor

    # Generate simple matched filter using reference chirp
    band_sz = int(bandwidth * fft_len / fs) // 2
    fc_baseband_bin = int(fc_baseband * fft_len / fs) // 2
    match_filter = np.fft.fft(ref_chirp, fft_len).conj().T
    match_filter[fc_baseband_bin - band_sz:fc_baseband_bin + band_sz] = 0
else:
    match_filter = loadReferenceChirp(debug_dir + '/' + fnme + '_Channel_2_MatchedFilter.dat')
    fft_len = len(match_filter)

# Load in cpi_length pulses of raw data
raw_data, pulse_range, attenuation, sys_time = \
   getRawData(parse_debug_dir + '/' + fnme + '_Radar_Data_Channel_2_Ka-Band.dat',
              cpi_length, 0, isIQ=True)
# raw_data, pulse_range, attenuation, sys_time = \
#   getRawData(debug_dir + '/' + fnme + '_Channel_2_Ka-Band_34_GHz_VV_RawData.dat',
#               cpi_length, 0, isIQ=True)

# Range compression
rc_data = np.fft.ifft(np.fft.fft(raw_data, fft_len, axis=0) * match_filter[:, None], axis=0)

# Doppler FFT of range compressed data
dopp_data = np.fft.fft(rc_data, axis=1)

# Plot results
plt.figure('Doppler Range Compressed Data')
plt.title('Pulses {}-{}'.format(0, cpi_length))
plt.imshow(np.fft.fftshift(db(dopp_data)), extent=[-dopp_prf / 2, dopp_prf / 2, far_range, near_range])
plt.ylabel('Slant Range (m)')
plt.xlabel('Doppler (Hz)')
plt.axis('tight')

plt.figure('Matched Filter')
plt.plot(db(match_filter))

